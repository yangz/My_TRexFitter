import logging
import pathlib

import ROOT

# atlas_* functions are adapted from ATLASUtil.py


def atlas_draw_ecm(x, y, ecm, color=1):
    atlas_draw_text(x, y, "#sqrt{s} = " + str(ecm) + " TeV", color, 0.035)
    return


def atlas_draw_luminosity(x, y, lumi, color=1):
    atlas_draw_text(x, y, "#intLdt = " + str(lumi) + " pb^{-1}", color, 0.05)
    return


def atlas_draw_luminosity_fb(x, y, lumi, energy, color=1):
    atlas_draw_text(x, y, str(energy) + " TeV, " + str(lumi) + " fb^{-1}", color, 0.035)
    return


def atlas_draw_text(
    x, y, text, color=1, size=0.04, NDC=True, halign="left", valign="bottom", angle=0.0
):
    objs = []
    skipLines = 0
    for line in text.split("\n"):
        objs.append(
            atlas_draw_text_one_line(
                x, y, line, color, size, NDC, halign, valign, skipLines, angle
            )
        )
        if NDC == True:
            y -= 0.05 * size / 0.04
        else:
            skipLines += 1
    return objs


def atlas_draw_text_one_line(
    x,
    y,
    text,
    color=1,
    size=0.04,
    NDC=True,
    halign="left",
    valign="bottom",
    skipLines=0,
    angle=0.0,
):
    halignMap = {"left": 1, "center": 2, "right": 3}
    valignMap = {"bottom": 1, "center": 2, "top": 3}
    scaleLineHeight = 1.0
    if valign == "top":
        scaleLineHeight = 0.8
    if skipLines:
        text = "#lower[%.1f]{%s}" % (skipLines * scaleLineHeight, text)
    # Draw the text quite simply:
    l = ROOT.TLatex()
    if NDC:
        l.SetNDC()
    l.SetTextAlign(10 * halignMap[halign] + valignMap[valign])
    l.SetTextColor(color)
    l.SetTextSize(size)
    l.SetTextAngle(angle)
    l.DrawLatex(x, y, text)
    return l


def atlas_label(x, y, color=1, plot_status="Internal"):
    l = ROOT.TLatex()
    l.SetNDC()
    l.SetTextFont(72)
    l.SetTextColor(color)
    l.DrawLatex(x, y, "ATLAS")
    l.SetTextFont(42)
    l.DrawLatex(x + 0.16, y, plot_status)
    return


def get_plot_color(id):
    color_dict = {
        0: 46,
        1: 42,
        2: 41,
        3: 30,
        4: 39,
        5: 38,
        6: 35,
        7: 48,
        8: 11,
        9: 12,
    }
    if id in color_dict:
        return color_dict[id]
    else:
        return 1


def parse_bool(plot_config, section, option):
    if plot_config.has_option(section, option):
        value = plot_config.get(section, option)
        if value:
            return plot_config.getboolean(section, option)
    return None


def parse_float(plot_config, section, option):
    if plot_config.has_option(section, option):
        value = plot_config.get(section, option)
        if value:
            return float(value)
    return None


def parse_float_list(plot_config, section, option):
    if plot_config.has_option(section, option):
        value = plot_config.get(section, option)
        if value:
            return [float(item.strip()) for item in value.split(",")]
    return []


def parse_int(plot_config, section, option):
    if plot_config.has_option(section, option):
        value = plot_config.get(section, option)
        if value:
            return int(value)
    return None


def parse_str(plot_config, section, option):
    if plot_config.has_option(section, option):
        value = plot_config.get(section, option)
        value = value.strip(r"""'" """)
        if value:
            return value
    return None


def parse_str_list(plot_config, section, option):
    if plot_config.has_option(section, option):
        value = plot_config.get(section, option)
        if value:
            return [item.strip() for item in value.split(",")]
    return []


def process_input(plot_config):
    """Checks & gets input numbers"""
    input_dict = {}
    x_points = parse_float_list(plot_config, "INPUT", "x_points")
    if not x_points:
        logging.critical("No x_points provided!")
        exit()
    else:
        input_dict["x_points"] = x_points
    input_dict["num_limits"] = len(x_points)
    input_dict["y_points_collect"] = []
    # check whether have TrexFitter input
    if parse_bool(plot_config, "TREX", "use_trex_input"):
        logging.info("Using limits input from TRexFitter...")
        path_prefix = parse_str(plot_config, "TREX", "path_prefix")
        path_suffix = parse_str(plot_config, "TREX", "path_suffix")
        tree_name = parse_str(plot_config, "TREX", "tree_name")
        folders = parse_str_list(plot_config, "TREX", "folders")
        medium_values = []
        plus1_values = []
        plus2_values = []
        minus1_values = []
        minus2_values = []
        obs_values = []
        for folder in folders:
            pre = pathlib.Path(path_prefix)
            res = list(pre.glob(folder + "/" + path_suffix))
            if len(res) == 0:
                logging.fatal(f"Can't find input file for {folder} !!!")
            elif len(res) > 1:
                logging.warn(f"More than 1 input file under {folder} was found, using the first one found. This may not be what you want!")
            limit_file = ROOT.TFile.Open(str(res[0]), "read")
            limit_tree = limit_file.Get(tree_name)
            for event in limit_tree:
                if parse_bool(plot_config, "TREX", "convert_to_coup"):
                    medium_values.append(coup(folder, event.exp_upperlimit))
                    plus1_values.append(coup(folder, event.exp_upperlimit_plus1))
                    plus2_values.append(coup(folder, event.exp_upperlimit_plus2))
                    minus1_values.append(coup(folder, event.exp_upperlimit_minus1))
                    minus2_values.append(coup(folder, event.exp_upperlimit_minus2))
                    obs_values.append(coup(folder, event.obs_upperlimit))
                else:
                    medium_values.append(event.exp_upperlimit)
                    plus1_values.append(event.exp_upperlimit_plus1)
                    plus2_values.append(event.exp_upperlimit_plus2)
                    minus1_values.append(event.exp_upperlimit_minus1)
                    minus2_values.append(event.exp_upperlimit_minus2)
                    obs_values.append(event.obs_upperlimit)
                break  # only 1 entry
        process_input_member_trex("upper_limits_medium", medium_values, input_dict)
        process_input_member_trex("upper_limits_plus1", plus1_values, input_dict)
        process_input_member_trex("upper_limits_plus2", plus2_values, input_dict)
        process_input_member_trex("upper_limits_minus1", minus1_values, input_dict)
        process_input_member_trex("upper_limits_minus2", minus2_values, input_dict)
        process_input_member_trex("upper_limits_observed", obs_values, input_dict)
    else:
        logging.info("Using limits input from config...")
        # shouldn't change order of process, otherwise y_points_collect will be wrong
        process_input_member(plot_config, "upper_limits_medium", input_dict)
        process_input_member(plot_config, "upper_limits_plus1", input_dict)
        process_input_member(plot_config, "upper_limits_plus2", input_dict)
        process_input_member(plot_config, "upper_limits_minus1", input_dict)
        process_input_member(plot_config, "upper_limits_minus2", input_dict)
        process_input_member(plot_config, "upper_limits_observed", input_dict)
    process_input_member(plot_config, "cross_sections", input_dict)

    return input_dict


def process_input_compare(plot_config):
    """Checks & gets input numbers"""
    input_dict = {}
    x_points = parse_float_list(plot_config, "INPUT", "x_points")
    if not x_points:
        logging.critical("No x_points provided!")
        exit()
    else:
        input_dict["x_points"] = x_points
    input_dict["num_limits"] = len(x_points)
    # check whether have TrexFitter input
    logging.info("Using limits input from config...")
    for i in range(10):
        input_name = "limits_compare_{}".format(i)
        process_input_member_compare(plot_config, input_name, input_dict)

    return input_dict


def process_input_member(plot_config, input_name, input_dict):
    """Check whether the quantity of x_points and processed input member is consisted"""
    if "x_points" not in input_dict:
        logging.critical("Please process x_points first!")
        exit()
    else:
        num_limits = input_dict["num_limits"]
    input_member = parse_float_list(plot_config, "INPUT", input_name)
    if not input_member:
        logging.warning("No {} provided, will not include!".format(input_name))
        input_dict["y_points_collect"].append(input_dict["upper_limits_medium"])
    else:
        input_dict[input_name] = input_member
        input_dict["y_points_collect"].append(input_dict[input_name])
        if len(input_member) != num_limits:
            logging.error(
                "Quantity of {} is not consistent with x_points, please check!".format(
                    input_name
                )
            )
            exit()


def process_input_member_compare(plot_config, input_name, input_dict):
    """Check whether the quantity of x_points and processed input member is consisted"""
    if "x_points" not in input_dict:
        logging.critical("Please process x_points first!")
        exit()
    else:
        num_limits = input_dict["num_limits"]
    input_member = parse_float_list(plot_config, "INPUT", input_name)
    if input_member:
        input_dict[input_name] = input_member
        input_dict[input_name + "_name"] = parse_str(
            plot_config, "INPUT", input_name + "_name"
        )
        if len(input_member) != num_limits:
            logging.error(
                "Quantity of {} is not consistent with x_points, please check!".format(
                    input_name
                )
            )
            exit()


def process_input_member_trex(input_name, input_values, input_dict):
    """Check whether the quantity of x_points and processed input member is consisted"""
    if "x_points" not in input_dict:
        logging.critical("Please process x_points first!")
        exit()
    else:
        num_limits = input_dict["num_limits"]
    if not input_values:
        logging.warning("No {} provided, will not include!".format(input_name))
        input_dict["y_points_collect"].append(input_dict["upper_limits_medium"])
    else:
        input_dict[input_name] = input_values
        input_dict["y_points_collect"].append(input_dict[input_name])
        if len(input_values) != num_limits:
            logging.error(
                "Quantity of {} is not consistent with x_points, please check!".format(
                    input_name
                )
            )
            exit()

def coup(mass_name, x):
    para_dict = {
        "m_05": [
            0.00014285959775009547,
            0.0030375747822960638,
            -0.0009267506934122755,
            0.0001709391854261289,
            -1.520500429564142e-05,
            5.129588006241013e-07,
        ],
        "m_07": [
            0.00017595453584282666,
            0.004506350002712834,
            -0.0019296193555482387,
            0.0005014119356643884,
            -6.288800567329947e-05,
            2.9924218006655305e-06,
        ],
        "m_09": [
            0.0002054556588638739,
            0.005968102987335838,
            -0.003208684966918542,
            0.0010498981243397564,
            -0.00016593297391993023,
            9.952291054296552e-06,
        ],
        "m_11": [
            0.00023434955540410396,
            0.007533804903872736,
            -0.004859314956131867,
            0.0019123048399373303,
            -0.000363726434025221,
            2.6260005862293543e-05,
        ],
        "m_13": [
            0.00026372453497646104,
            0.00927221893943123,
            -0.007012556831871669,
            0.0032431471878829783,
            -0.0007253313488181435,
            6.158957856185753e-05,
        ],
        "m_15": [
            0.00030140131268462095,
            0.009928707323140365,
            -0.0066873563929840596,
            0.0027497376729996767,
            -0.0005465517023259065,
            4.123766016733218e-05,
        ],
        "m_17": [
            0.00034102924027640464,
            0.010778930231433228,
            -0.006745991587815559,
            0.0025747926073592577,
            -0.0004749596227248707,
            3.325787992210886e-05,
        ],
        "m_19": [
            0.0003829175036602703,
            0.01182948238296452,
            -0.007104051922965414,
            0.002600374092165853,
            -0.00045994037888638515,
            3.087762082618056e-05,
        ],
        "m_23": [
            0.0004897939381709948,
            0.012330725436757579,
            -0.00511527756530375,
            0.0012872200675510146,
            -0.00015632526205605987,
            7.20212113738297e-06,
        ],
        "m_27": [
            0.0006125885457362561,
            0.014177661834207189,
            -0.005057743806702551,
            0.0010925494700167217,
            -0.00011385855228070985,
            4.501105148540806e-06,
        ],
        "m_31": [
            0.0007565984105016778,
            0.017291151086781597,
            -0.006016140173792976,
            0.0012671067367092646,
            -0.0001287343966321599,
            4.96102441157784e-06,
        ],
        "m_35": [
            0.0009613221073674271,
            0.01782606670819882,
            -0.0042425676872327065,
            0.0006089171088035651,
            -4.211651976156558e-05,
            1.1045069428457266e-06,
        ],
        "m_39": [
            0.0012016009128179765,
            0.020711219989254324,
            -0.004287988552453745,
            0.0005346100194725886,
            -3.2107742086009534e-05,
            7.310223187075262e-07,
        ],
        "m_42": [
            0.001413639665734382,
            0.025661920212525682,
            -0.005859087350315529,
            0.0008064572530198843,
            -5.34920003945024e-05,
            1.3453290734789734e-06,
        ],
        "m_42_low": [
            0.001413639665734382,
            0.025661920212525682,
            -0.005859087350315529,
            0.0008064572530198843,
            -5.34920003945024e-05,
            1.3453290734789734e-06,
        ],
        "m_42_mz2": [
            0.001413639665734382,
            0.025661920212525682,
            -0.005859087350315529,
            0.0008064572530198843,
            -5.34920003945024e-05,
            1.3453290734789734e-06,
        ],
        "m_45": [
            0.0016642885386911354,
            0.03242981601260141,
            -0.008434036788708524,
            0.0013240818801412155,
            -0.00010020248969094798,
            2.8754708886295387e-06,
        ],
        "m_48": [
            0.001966551519235829,
            0.0417826831724651,
            -0.012756596725438874,
            0.002354051096270498,
            -0.00020945475864435754,
            7.068112344262865e-06,
        ],
        "m_51": [
            0.0023224160441653713,
            0.0548259786981602,
            -0.020176464093835427,
            0.0044964723204448526,
            -0.00048341538776611675,
            1.9714058629911877e-05,
        ],
        "m_54": [
            0.002847330784353659,
            0.06015782161296643,
            -0.01817079334614929,
            0.003315620739819685,
            -0.0002916616710874115,
            9.729431862949621e-06,
        ],
        "m_57": [
            0.0034543530094960726,
            0.0707360500652855,
            -0.02008497570213265,
            0.00344342948855043,
            -0.00028454653591703807,
            8.915809182644659e-06,
        ],
        "m_60": [
            0.00424375697336179,
            0.07870359839248324,
            -0.018596062522632132,
            0.002648271366294158,
            -0.00018170308812117926,
            4.72598956012328e-06,
        ],
        "m_63": [
            0.005183227768423572,
            0.09311311917943178,
            -0.020787111897707888,
            0.002795867443978309,
            -0.00018125064871498723,
            4.454404106768783e-06,
        ],
        "m_66": [
            0.006256437629747566,
            0.11337448540561064,
            -0.02555348300915392,
            0.003463804281430823,
            -0.00022623427092564134,
            5.598118051902577e-06,
        ],
        "m_69": [
            0.007437009700996319,
            0.13871544818408635,
            -0.03256666852093385,
            0.004587420176565141,
            -0.0003113237247549025,
            7.997652032141912e-06,
        ],
        "m_72": [
            0.008670962916577311,
            0.16656136940447963,
            -0.04063251016443493,
            0.005931787980697107,
            -0.0004175042062685075,
            1.1116761369568317e-05,
        ],
        "m_75": [
            0.009854199243512225,
            0.1942626498410473,
            -0.04854993911222515,
            0.007224562470082335,
            -0.0005178731029686154,
            1.4011994863249172e-05,
        ],
        "m_10": [
            0.0002227282421805712,
            0.006264483507441368,
            -0.0031781875545881964,
            0.0009805232453748824,
            -0.00014609179976095968,
            8.259938550199512e-06,
        ],
    }
    p = para_dict[mass_name]
    return (
        p[0] + p[1] * x + p[2] * x ** 2 + p[3] * x ** 3 + p[4] * x ** 4 + p[5] * x ** 5
    )