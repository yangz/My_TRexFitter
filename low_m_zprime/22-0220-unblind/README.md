# Internal Checks on Unblind Results

## settings

* mass window: 3 sigma with **UPDATED** quad fit method

## Plots

* p_values:

    ```bash
    cd sys
    conda activate pyroot
    source fit_pvalues.py
    python get_pvalues.py
    ```
