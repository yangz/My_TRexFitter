import pathlib
from shutil import copyfile

from settings import *
from workspace_const import p4_sys_names

with open("config_template.config", "r") as f:
    config_temp = f.read()
with open("config_template_wt_sys_module.config", "r") as f:
    config_wt_sys_temp = f.read()
with open("config_template_p4_sys_module.config", "r") as f:
    config_p4_sys_temp = f.read()

for mass in mass_points_low + mass_points_high:
    if mass in mass_points_low:
        region = "low_mass"
        fit_var = "mz2"
        p_cr_low=0
        p_cr_high=80
    elif mass in mass_points_high:
        region = "high_mass"
        fit_var = "mz1"
        p_cr_low=30
        p_cr_high=120
    else:
        print("#### ERROR! Unknown mass!")
        exit(1)
    dnn_cut = dnn_cut_dict[mass]
    if mass == "42_mz2":
        mass_str = "m_42_mz2"
        fit_var = "mz2"
        mass = 42
    else:
        mass_str = f"m_{mass:02d}"

    # prepare folders
    stats_dir = pathlib.Path(f"stats/{mass_str}")
    stats_dir.mkdir(parents=True, exist_ok=True)
    sys_dir = pathlib.Path(f"sys/{mass_str}")
    sys_dir.mkdir(parents=True, exist_ok=True)
    m_low, m_high = get_mass_cut(mass)
    bin_sr, bin_cr = get_bin_dict(mass)
    for ky, sr_bins in bin_sr.items():
        cr_bins = bin_cr[ky]
        dnn_label = f"{int(dnn_cut*100):02d}"
        # write config without sys
        if "auto" in ky:
            new_config_stats = config_temp.format(
                p_job=ky,
                p_fit_type="stats",
                p_mass=mass,
                p_fit_var=fit_var,
                p_ntuple_path=f"{fit_ntup_dir}/{region}/tree_NOMINAL",
                p_mass_cut_low=m_low,
                p_mass_cut_high=m_high,
                p_dnn_cut=dnn_cut,
                p_dnn_cut_label=dnn_label,
                p_region=region,
                p_window=m_high - m_low,
                p_cr_low=p_cr_low,
                p_cr_high=p_cr_high - (m_high - m_low),
                p_cr_bin=10,
                p_sr_bin=10,
                p_binning_cr=cr_bins,
                p_binning_sr=sr_bins,
            )
        else:
            new_config_stats = config_temp.format(
                p_job=ky,
                p_fit_type="stats",
                p_mass=mass,
                p_fit_var=fit_var,
                p_ntuple_path=f"{fit_ntup_dir}/{region}/tree_NOMINAL",
                p_mass_cut_low=m_low,
                p_mass_cut_high=m_high,
                p_dnn_cut=dnn_cut,
                p_dnn_cut_label=dnn_label,
                p_region=region,
                p_window=m_high - m_low,
                p_cr_low=p_cr_low,
                p_cr_high=p_cr_high - (m_high - m_low),
                p_cr_bin=cr_bins,
                p_sr_bin=sr_bins,
                p_binning_cr="",
                p_binning_sr="",
            )
        config_name = f"m_{mass:03d}_{ky}_stats.config"
        with stats_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
            f.write(new_config_stats)

        # prepare sys config
        # set nominal config
        if "auto" in ky:
            new_config = config_temp.format(
                p_job=ky,
                p_fit_type="sys",
                p_mass=mass,
                p_fit_var=fit_var,
                p_ntuple_path=f"{fit_ntup_dir}/{region}/tree_NOMINAL",
                p_mass_cut_low=m_low,
                p_mass_cut_high=m_high,
                p_dnn_cut=dnn_cut,
                p_dnn_cut_label=dnn_label,
                p_region=region,
                p_window=m_high - m_low,
                p_cr_low=p_cr_low,
                p_cr_high=p_cr_high - (m_high - m_low),
                p_cr_bin=10,
                p_sr_bin=10,
                p_binning_cr=cr_bins,
                p_binning_sr=sr_bins,
            )
        else:
            new_config = config_temp.format(
                p_job=ky,
                p_fit_type="sys",
                p_mass=mass,
                p_fit_var=fit_var,
                p_ntuple_path=f"{fit_ntup_dir}/{region}/tree_NOMINAL",
                p_mass_cut_low=m_low,
                p_mass_cut_high=m_high,
                p_dnn_cut=dnn_cut,
                p_dnn_cut_label=dnn_label,
                p_region=region,
                p_window=m_high - m_low,
                p_cr_low=p_cr_low,
                p_cr_high=p_cr_high - (m_high - m_low),
                p_cr_bin=cr_bins,
                p_sr_bin=sr_bins,
                p_binning_cr="%",
                p_binning_sr="%",
            )

        # add weight systematic config
        sys_config = ""
        config_wt_config = config_wt_sys_temp.format(
            p_mass=mass,
            p_ntuple_path=f"{fit_ntup_dir}/{region}/tree_NOMINAL",
            p_region=region,
            p_qcd_var=fit_var,
        )
        sys_config += config_wt_config
        # add p4 systematic config
        for sys_name in p4_sys_names:
            ntuple_path_up_sig = f"{fit_ntup_dir}/{region}/tree_{sys_name}up"
            ntuple_path_down_sig = f"{fit_ntup_dir}/{region}/tree_{sys_name}down"
            # sig
            sys_entry = config_p4_sys_temp.format(
                p_sys_name=sys_name[5:],
                p_sample="Zprime",
                p_ntuple_path_up=ntuple_path_up_sig,
                p_ntuple_path_down=ntuple_path_down_sig,
                p_ntuple_files=f"sig_Zp{mass:03d}",
                p_weight_str="weight",
            )
            sys_config += sys_entry
            # bkg
            ntuple_path_up_bkg = f"{fit_ntup_dir}/{region}/tree_{sys_name}up"
            ntuple_path_down_bkg = f"{fit_ntup_dir}/{region}/tree_{sys_name}down"
            ## add qcd
            sys_entry = config_p4_sys_temp.format(
                p_sys_name=sys_name,
                p_sample="ZZ4l",
                p_ntuple_path_up=ntuple_path_up_bkg,
                p_ntuple_path_down=ntuple_path_down_bkg,
                p_ntuple_files="bkg_qcd",
                p_weight_str="weight",
            )
            sys_config += sys_entry
            ## add ggZZ
            sys_entry = config_p4_sys_temp.format(
                p_sys_name=sys_name,
                p_sample="ggZZ",
                p_ntuple_path_up=ntuple_path_up_bkg,
                p_ntuple_path_down=ntuple_path_down_bkg,
                p_ntuple_files="bkg_ggZZ",
                p_weight_str="weight",
            )
            sys_config += sys_entry
        new_config += sys_config
        # write config with sys
        config_name = f"m_{mass:03d}_{ky}_sys.config"
        with sys_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
            f.write(new_config)

    fit_script_name = "fit_all.sh"
    copyfile(f"./{fit_script_name}", stats_dir.joinpath(fit_script_name))
    copyfile(f"./{fit_script_name}", sys_dir.joinpath(fit_script_name))

fit_script_name = "get_limits.py"
copyfile(f"./{fit_script_name}", pathlib.Path(f"stats").joinpath(fit_script_name))
copyfile(f"./{fit_script_name}", pathlib.Path(f"sys").joinpath(fit_script_name))
