import matplotlib.pyplot as plt


def lin_mass_sigma(mass):
    return -0.0495893 + 0.021717 * mass


def quad_mass_sigma(mass):
    return 0.0401444 + 0.0122949 * mass + 0.000140695 * mass * mass


mass_points_low = [5, 7, 9, 11, 13, 15, 17, 19, 23, 27, 31, 35, 39]
mass_points_high = [42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75]
mass_points = mass_points_low + mass_points_high

lin_sigma = []
quad_sigma = []
for mass in mass_points:
    lin_sigma.append(lin_mass_sigma(mass))
    quad_sigma.append(quad_mass_sigma(mass))

fig, ax = plt.subplots()
ax.plot(mass_points, lin_sigma, label="lin")
ax.scatter(mass_points, lin_sigma, label="lin")
ax.plot(mass_points, quad_sigma, label="quad")
ax.scatter(mass_points, quad_sigma, label="quad")
ax.set_xlabel("mass [GeV]")
ax.set_ylabel("sigma [GeV]")
ax.legend()

fig.savefig("plot_sigma.png")