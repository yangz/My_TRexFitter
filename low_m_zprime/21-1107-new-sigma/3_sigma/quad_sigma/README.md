# Extra contents

* 3 sigma with quad fit method is the default method before pre-approval

* check the extended fit of low mass (fit mz2 up to 51 GeV)

* check the extended fit of high mass (fit mz1 down to 31 GeV)