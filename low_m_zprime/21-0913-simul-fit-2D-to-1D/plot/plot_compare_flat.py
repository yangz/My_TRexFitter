import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import numpy as np

ampl.use_atlas_style(usetex=False)


mass_points = [
    5,
    7,
    9,
    11,
    13,
    15,
    17,
    19,
    23,
    27,
    31,
    35,
    39,
    42,
    45,
    48,
    51,
    54,
    57,
    60,
    63,
    66,
    69,
    72,
    75,
]
exp_limits_dnn_1d = [
    0.002378545,
    0.002889704,
    0.003259255,
    0.003519453,
    0.003786972,
    0.003902442,
    0.004060433,
    0.004239164,
    0.004484654,
    0.004886385,
    0.005611997,
    0.006121026,
    0.007584023,
    0.011192193,
    0.011765344,
    0.014800446,
    0.019431214,
    0.02357105,
    0.030785777,
    0.041193328,
    0.060820734,
    0.086927826,
    0.121405585,
    0.140610679,
    0.166358368,
]
exp_limits_dnn_2d = [
    0.002378545,
    0.002889704,
    0.003259255,
    0.003519453,
    0.003786972,
    0.003902442,
    0.004060433,
    0.004239164,
    0.004484654,
    0.004886385,
    0.005611997,
    0.006121026,
    0.007584023,
    0.011192193,
    0.011765344,
    0.014800446,
    0.019431214,
    0.02332,
    0.03049,
    0.04031,
    0.05948,
    0.08523,
    0.1175,
    0.1379,
    0.1659,
]

fig, ax = plt.subplots()

ax.plot(
    mass_points,
    exp_limits_dnn_1d,
    color="tan",
    linestyle="-",
    label="ATLAS exp. (1D)",
)

ax.plot(
    mass_points,
    exp_limits_dnn_2d,
    color="black",
    linestyle="--",
    label="ATLAS exp. (2D, m >= 54)",
)
ax.legend(loc="lower right", facecolor="white", edgecolor="black", framealpha=1.0)

fig.savefig("limit_compare_lin.png")
fig.savefig("limit_compare_lin.svg")
