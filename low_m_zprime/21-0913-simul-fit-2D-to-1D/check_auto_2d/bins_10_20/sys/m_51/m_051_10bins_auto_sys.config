% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "job_m_051_10bins_auto_sys"
  CmeLabel: "13 TeV"
  POI: "g"
  ReadFrom: NTUP
  NtuplePaths: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  Label: "Four Muon"
  LumiLabel: "140.0 fb^{-1}"    % data17?
  % Lumi: 140
  NtupleName: "ntup"
  DebugLevel: 0
  MCstatThreshold: 5%
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  

% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRSR
  FitType: SPLUSB
  POIAsimov: 0
  FitBlind: TRUE
  doLHscan: "g"

% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
  SignalInjection: FALSE
  SignalInjectionValue: 1.0


% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: TRUE
  POIAsimov: 1


% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ZZ4l_CR"
  Type: CONTROL
  Selection: "((mz1 > 53.8586868) || (mz1 < 48.1413132) || (mz2 > 53.8586868) || (mz2 < 48.1413132)) && (dnn_out_sig > 0.14)"
  Variable: "int((mz1 - 30) / 2) * 40 + int(mz2/2)", 2200, 0, 2200
  VariableTitle: "(mz1, mz2)"
  Binning: "AutoBin","TransfoD", 5, 5
  Label: "Control Region"
  ShortLabel: "CR,4muon"

Region: "Zprime_SR"
  Type: SIGNAL
  Selection: "((mz1 > 48.1413132) && (mz1 < 53.8586868) && (mz2 > 48.1413132) && (mz2 < 53.8586868)) && (dnn_out_sig > 0.14)"
  Variable: "int(((mz1 - 48.1413132) * 110 / 5.717373600000002 - 30) / 2) * 40 + int((mz2 - 48.1413132) * 110 / 5.717373600000002/2)", 2200, 0, 2200
  VariableTitle: "(mz1, mz2)"
  Binning: "AutoBin","TransfoD", 5, 5
  Label: "Signal Region"
  ShortLabel: "SR,4muon"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "Zprime"
  Type: SIGNAL
  Title: "Zprime"
  FillColor: 2
  LineColor: 2
  NtupleFile: "sig_Zp051"
  MCweight: "weight"

Sample: "ZZ4l"
  Type: BACKGROUND
  Title: "ZZ4l"
  FillColor: 4
  LineColor: 4
  NtupleFile: "bkg_qcd"
  MCweight: "weight"

Sample: "ggZZ"
  Type: BACKGROUND
  Title: "ggZZ"
  FillColor: 434
  LineColor: 434
  NtupleFile: "bkg_ggZZ"
  MCweight: "weight"

Sample: "fakes_data"
  Type: BACKGROUND
  Title: "fakes_data"
  FillColorRGB: 244, 121, 66
  LineColorRGB: 0, 0, 0
  NtupleFiles: "bkg_fakes_data"
  MCweight: "weight"

Sample: "fakes_diboson"
  Type: BACKGROUND
  Title: "fakes_diboson"
  FillColor: 880
  LineColor: 880
  NtupleFiles: "bkg_fakes_diboson"
  MCweight: "weight"

%Sample: "DD"
%  Type: BACKGROUND
%  Title: "DD"
%  FillColor: 434
%  LineColor: 434
%  NtupleFile: "tree_DD"
%  MCweight: "weight"
%  NormalizedByTheory: FALSE

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "g"
  Title: "#sigma(fb)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: Zprime

NormFactor: "muZZ4l"
  Title: "#mu (ZZ4l)"
  Min: 0
  Max: 10
  Nominal: 1
  % Constant: TRUE
  Samples: ZZ4l

% NormFactor: "muLumi"
%   Title: "Lumi Scale"
%   Min: 0
%   Max: 100
%   Nominal: 1
%   Constant: TRUE
%   Samples: all


% --------------- %
% - SYSTEMATICS - %
% --------------- %

%% Overall Systematics

  Systematic: "LUMI"
    Title: "Luminosity"
    Type: OVERALL
    Samples: all
    OverallUp: 0.021
    OverallDown: -0.021
    Category: Lumi

%% weight Systematics

  % QCD %

  %Systematic: "weight_QCD_SCALE"
  %  Title: "weight_QCD_SCALE"
  %  Type: HISTO
  %  Samples: ZZ4l
  %  Regions: ZZ4l_CR, Zprime_SR
  %  NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtupleFilesUp: "bkg_qcd"
  %  NtupleFilesDown: "bkg_qcd"
  %  NtupleNameUp: "ntup"
  %  NtupleNameDown: "ntup"
  %  Symmetrisation: TWOSIDED
  %  WeightUp: weight_qcd_scale_up_mz1
  %  WeightDown: weight_qcd_scale_down_mz1

  Systematic: "weight_QCD_SCALE"
    Title: "weight_QCD_SCALE"
    Type: OVERALL
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    OverallUp: 0.08
    OverallDown: 0.08
    Category: Theory


  % PDF %

  %Systematic: "weight_PDF_UP"
  %  Title: "weight_PDF_UP"
  %  Type: HISTO
  %  Samples: ZZ4l
  %  Regions: ZZ4l_CR, Zprime_SR
  %  NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtupleFilesUp: "bkg_qcd"
  %  NtupleNameUp: "ntup"
  %  Symmetrisation: ONESIDED
  %  WeightUp: weight_pdf_up_mz1
  
  Systematic: "weight_PDF"
    Title: "weight_PDF"
    Type: OVERALL
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    OverallUp: 0.02
    OverallDown: 0.02
    Category: Theory

  % Alpha S %

  %Systematic: "weight_ALPHA_S_UP"
  %  Title: "weight_ALPHA_S_UP"
  %  Type: HISTO
  %  Samples: ZZ4l
  %  Regions: ZZ4l_CR, Zprime_SR
  %  NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtupleFilesUp: "bkg_qcd"
  %  NtupleNameUp: "ntup"
  %  Symmetrisation: ONESIDED
  %  WeightUp: weight_alpha_s_up_mz1
  
  Systematic: "weight_ALPHA_S"
    Title: "weight_ALPHA_S"
    Type: OVERALL
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    OverallUp: 0.015
    OverallDown: 0.015
    Category: Theory


%% p4 Systematics

  Systematic: "F_B_systematics__1"
    Title: "F_B_systematics__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_B_systematics__1"
    Title: "FT_EFF_B_systematics__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_B_systematics__1"
    Title: "FT_EFF_B_systematics__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_C_systematics__1"
    Title: "F_C_systematics__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_C_systematics__1"
    Title: "FT_EFF_C_systematics__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_C_systematics__1"
    Title: "FT_EFF_C_systematics__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_Light_systematics__1"
    Title: "F_Light_systematics__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_Light_systematics__1"
    Title: "FT_EFF_Light_systematics__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_Light_systematics__1"
    Title: "FT_EFF_Light_systematics__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_extrapolation__1"
    Title: "F_extrapolation__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation__1"
    Title: "FT_EFF_extrapolation__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation__1"
    Title: "FT_EFF_extrapolation__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_extrapolation_from_charm__1"
    Title: "F_extrapolation_from_charm__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation_from_charm__1"
    Title: "FT_EFF_extrapolation_from_charm__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation_from_charm__1"
    Title: "FT_EFF_extrapolation_from_charm__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_ISO_STAT__1"
    Title: "EFF_ISO_STAT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_STAT__1"
    Title: "MUON_EFF_ISO_STAT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_STAT__1"
    Title: "MUON_EFF_ISO_STAT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_ISO_SYS__1"
    Title: "EFF_ISO_SYS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_SYS__1"
    Title: "MUON_EFF_ISO_SYS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_SYS__1"
    Title: "MUON_EFF_ISO_SYS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_STAT__1"
    Title: "EFF_RECO_STAT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT__1"
    Title: "MUON_EFF_RECO_STAT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT__1"
    Title: "MUON_EFF_RECO_STAT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_STAT_LOWPT__1"
    Title: "EFF_RECO_STAT_LOWPT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT_LOWPT__1"
    Title: "MUON_EFF_RECO_STAT_LOWPT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT_LOWPT__1"
    Title: "MUON_EFF_RECO_STAT_LOWPT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_SYS__1"
    Title: "EFF_RECO_SYS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS__1"
    Title: "MUON_EFF_RECO_SYS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS__1"
    Title: "MUON_EFF_RECO_SYS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_SYS_LOWPT__1"
    Title: "EFF_RECO_SYS_LOWPT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS_LOWPT__1"
    Title: "MUON_EFF_RECO_SYS_LOWPT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS_LOWPT__1"
    Title: "MUON_EFF_RECO_SYS_LOWPT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_TTVA_STAT__1"
    Title: "EFF_TTVA_STAT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_STAT__1"
    Title: "MUON_EFF_TTVA_STAT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_STAT__1"
    Title: "MUON_EFF_TTVA_STAT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_TTVA_SYS__1"
    Title: "EFF_TTVA_SYS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_SYS__1"
    Title: "MUON_EFF_TTVA_SYS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_SYS__1"
    Title: "MUON_EFF_TTVA_SYS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "ID__1"
    Title: "ID__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_ID__1"
    Title: "MUON_ID__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_ID__1"
    Title: "MUON_ID__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MS__1"
    Title: "MS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_MS__1"
    Title: "MUON_MS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_MS__1"
    Title: "MUON_MS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "SAGITTA_RESBIAS__1"
    Title: "SAGITTA_RESBIAS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RESBIAS__1"
    Title: "MUON_SAGITTA_RESBIAS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RESBIAS__1"
    Title: "MUON_SAGITTA_RESBIAS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "SAGITTA_RHO__1"
    Title: "SAGITTA_RHO__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RHO__1"
    Title: "MUON_SAGITTA_RHO__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RHO__1"
    Title: "MUON_SAGITTA_RHO__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "SCALE__1"
    Title: "SCALE__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SCALE__1"
    Title: "MUON_SCALE__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SCALE__1"
    Title: "MUON_SCALE__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "ATASF__1"
    Title: "ATASF__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1down"
    NtupleFilesUp: "sig_Zp051"
    NtupleFilesDown: "sig_Zp051"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "PRW_DATASF__1"
    Title: "PRW_DATASF__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "PRW_DATASF__1"
    Title: "PRW_DATASF__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

