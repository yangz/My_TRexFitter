% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "job_m_072_20bins_sys"
  CmeLabel: "13 TeV"
  POI: "g"
  ReadFrom: NTUP
  NtuplePaths: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  Label: "Four Muon"
  LumiLabel: "140.0 fb^{-1}"    % data17?
  % Lumi: 140
  NtupleName: "ntup"
  DebugLevel: 0
  MCstatThreshold: 5%
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  

% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRSR
  FitType: SPLUSB
  POIAsimov: 0
  FitBlind: TRUE
  doLHscan: "g"

% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
  SignalInjection: FALSE
  SignalInjectionValue: 1.0


% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: TRUE
  POIAsimov: 1


% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ZZ4l_CR"
  Type: CONTROL
  Selection: "((mz1 > 76.0608654) || (mz1 < 67.9391346)) && (dnn_out_sig > 0.2)"
  Variable: "mz1",10,0,80
  VariableTitle: "mz1"
  Binning: 0.0, 3.3969567300000003, 6.793913460000001, 10.190870190000002, 13.587826920000001, 16.98478365, 20.381740380000004, 23.778697110000003, 27.175653840000003, 30.572610570000002, 33.9695673, 37.36652403, 40.76348076000001, 44.16043749000001, 47.557394220000006, 50.954350950000006, 54.351307680000005, 57.748264410000004, 61.145221140000004, 64.54217787, 67.9391346, 76.0608654, 76.25782213, 76.45477886, 76.65173559, 76.84869232, 77.04564905, 77.24260577999999, 77.43956251, 77.63651924, 77.83347597, 78.0304327, 78.22738943, 78.42434616, 78.62130289, 78.81825961999999, 79.01521635, 79.21217308, 79.40912981, 79.60608654, 79.80304327, 80.0
  Label: "Control Region"
  ShortLabel: "CR,4muon"

Region: "Zprime_SR"
  Type: SIGNAL
  Selection: "((mz1 > 67.9391346) && (mz1 < 76.0608654)) && (dnn_out_sig > 0.2)"
  Variable: "mz1",5,67.9391346,76.0608654
  VariableTitle: "mz1"
  Binning: 67.9391346, 68.34522114, 68.75130768, 69.15739422, 69.56348076, 69.9695673, 70.37565384, 70.78174038, 71.18782692, 71.59391346, 72.0, 72.40608654, 72.81217308, 73.21825962, 73.62434616, 74.0304327, 74.43651924, 74.84260578, 75.24869232, 75.65477886, 76.0608654
  Label: "Signal Region"
  ShortLabel: "SR,4muon"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "Zprime"
  Type: SIGNAL
  Title: "Zprime"
  FillColor: 2
  LineColor: 2
  NtupleFile: "sig_Zp072"
  MCweight: "weight"

Sample: "ZZ4l"
  Type: BACKGROUND
  Title: "ZZ4l"
  FillColor: 4
  LineColor: 4
  NtupleFile: "bkg_qcd"
  MCweight: "weight"

Sample: "ggZZ"
  Type: BACKGROUND
  Title: "ggZZ"
  FillColor: 434
  LineColor: 434
  NtupleFile: "bkg_ggZZ"
  MCweight: "weight"

Sample: "fakes"
  Type: BACKGROUND
  Title: "fakes"
  FillColor: 880
  LineColor: 880
  NtupleFiles: "bkg_fakes_diboson","bkg_fakes_ttbar","bkg_fakes_zll"
  MCweight: "weight"

%Sample: "DD"
%  Type: BACKGROUND
%  Title: "DD"
%  FillColor: 434
%  LineColor: 434
%  NtupleFile: "tree_DD"
%  MCweight: "weight"
%  NormalizedByTheory: FALSE

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "g"
  Title: "#sigma(fb)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: Zprime

NormFactor: "muZZ4l"
  Title: "#mu (ZZ4l)"
  Min: 0
  Max: 10
  Nominal: 1
  % Constant: TRUE
  Samples: ZZ4l

% NormFactor: "muLumi"
%   Title: "Lumi Scale"
%   Min: 0
%   Max: 100
%   Nominal: 1
%   Constant: TRUE
%   Samples: all


% --------------- %
% - SYSTEMATICS - %
% --------------- %

%% Overall Systematics

  Systematic: "LUMI"
    Title: "Luminosity"
    Type: OVERALL
    Samples: all
    OverallUp: 0.021
    OverallDown: -0.021
    Category: Lumi

%% weight Systematics

  % QCD %

  %Systematic: "weight_QCD_SCALE"
  %  Title: "weight_QCD_SCALE"
  %  Type: HISTO
  %  Samples: ZZ4l
  %  Regions: ZZ4l_CR, Zprime_SR
  %  NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtupleFilesUp: "bkg_qcd"
  %  NtupleFilesDown: "bkg_qcd"
  %  NtupleNameUp: "ntup"
  %  NtupleNameDown: "ntup"
  %  Symmetrisation: TWOSIDED
  %  WeightUp: weight_qcd_scale_up_mz1
  %  WeightDown: weight_qcd_scale_down_mz1

  Systematic: "weight_QCD_SCALE"
    Title: "weight_QCD_SCALE"
    Type: OVERALL
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    OverallUp: 0.08
    OverallDown: 0.08
    Category: Theory


  % PDF %

  %Systematic: "weight_PDF_UP"
  %  Title: "weight_PDF_UP"
  %  Type: HISTO
  %  Samples: ZZ4l
  %  Regions: ZZ4l_CR, Zprime_SR
  %  NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtupleFilesUp: "bkg_qcd"
  %  NtupleNameUp: "ntup"
  %  Symmetrisation: ONESIDED
  %  WeightUp: weight_pdf_up_mz1
  
  Systematic: "weight_PDF"
    Title: "weight_PDF"
    Type: OVERALL
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    OverallUp: 0.02
    OverallDown: 0.02
    Category: Theory

  % Alpha S %

  %Systematic: "weight_ALPHA_S_UP"
  %  Title: "weight_ALPHA_S_UP"
  %  Type: HISTO
  %  Samples: ZZ4l
  %  Regions: ZZ4l_CR, Zprime_SR
  %  NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  %  NtupleFilesUp: "bkg_qcd"
  %  NtupleNameUp: "ntup"
  %  Symmetrisation: ONESIDED
  %  WeightUp: weight_alpha_s_up_mz1
  
  Systematic: "weight_ALPHA_S"
    Title: "weight_ALPHA_S"
    Type: OVERALL
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    OverallUp: 0.015
    OverallDown: 0.015
    Category: Theory


%% p4 Systematics

  Systematic: "F_B_systematics__1"
    Title: "F_B_systematics__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_B_systematics__1"
    Title: "FT_EFF_B_systematics__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_B_systematics__1"
    Title: "FT_EFF_B_systematics__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_B_systematics__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_C_systematics__1"
    Title: "F_C_systematics__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_C_systematics__1"
    Title: "FT_EFF_C_systematics__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_C_systematics__1"
    Title: "FT_EFF_C_systematics__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_C_systematics__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_Light_systematics__1"
    Title: "F_Light_systematics__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_Light_systematics__1"
    Title: "FT_EFF_Light_systematics__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_Light_systematics__1"
    Title: "FT_EFF_Light_systematics__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_Light_systematics__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_extrapolation__1"
    Title: "F_extrapolation__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation__1"
    Title: "FT_EFF_extrapolation__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation__1"
    Title: "FT_EFF_extrapolation__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "F_extrapolation_from_charm__1"
    Title: "F_extrapolation_from_charm__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation_from_charm__1"
    Title: "FT_EFF_extrapolation_from_charm__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "FT_EFF_extrapolation_from_charm__1"
    Title: "FT_EFF_extrapolation_from_charm__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_FT_EFF_extrapolation_from_charm__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_ISO_STAT__1"
    Title: "EFF_ISO_STAT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_STAT__1"
    Title: "MUON_EFF_ISO_STAT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_STAT__1"
    Title: "MUON_EFF_ISO_STAT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_STAT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_ISO_SYS__1"
    Title: "EFF_ISO_SYS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_SYS__1"
    Title: "MUON_EFF_ISO_SYS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_ISO_SYS__1"
    Title: "MUON_EFF_ISO_SYS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_ISO_SYS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_STAT__1"
    Title: "EFF_RECO_STAT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT__1"
    Title: "MUON_EFF_RECO_STAT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT__1"
    Title: "MUON_EFF_RECO_STAT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_STAT_LOWPT__1"
    Title: "EFF_RECO_STAT_LOWPT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT_LOWPT__1"
    Title: "MUON_EFF_RECO_STAT_LOWPT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_STAT_LOWPT__1"
    Title: "MUON_EFF_RECO_STAT_LOWPT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_STAT_LOWPT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_SYS__1"
    Title: "EFF_RECO_SYS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS__1"
    Title: "MUON_EFF_RECO_SYS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS__1"
    Title: "MUON_EFF_RECO_SYS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_RECO_SYS_LOWPT__1"
    Title: "EFF_RECO_SYS_LOWPT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS_LOWPT__1"
    Title: "MUON_EFF_RECO_SYS_LOWPT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_RECO_SYS_LOWPT__1"
    Title: "MUON_EFF_RECO_SYS_LOWPT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_RECO_SYS_LOWPT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_TTVA_STAT__1"
    Title: "EFF_TTVA_STAT__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_STAT__1"
    Title: "MUON_EFF_TTVA_STAT__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_STAT__1"
    Title: "MUON_EFF_TTVA_STAT__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_STAT__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "EFF_TTVA_SYS__1"
    Title: "EFF_TTVA_SYS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_SYS__1"
    Title: "MUON_EFF_TTVA_SYS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_EFF_TTVA_SYS__1"
    Title: "MUON_EFF_TTVA_SYS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_EFF_TTVA_SYS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "ID__1"
    Title: "ID__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_ID__1"
    Title: "MUON_ID__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_ID__1"
    Title: "MUON_ID__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_ID__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MS__1"
    Title: "MS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_MS__1"
    Title: "MUON_MS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_MS__1"
    Title: "MUON_MS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_MS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "SAGITTA_RESBIAS__1"
    Title: "SAGITTA_RESBIAS__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RESBIAS__1"
    Title: "MUON_SAGITTA_RESBIAS__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RESBIAS__1"
    Title: "MUON_SAGITTA_RESBIAS__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RESBIAS__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "SAGITTA_RHO__1"
    Title: "SAGITTA_RHO__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RHO__1"
    Title: "MUON_SAGITTA_RHO__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SAGITTA_RHO__1"
    Title: "MUON_SAGITTA_RHO__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SAGITTA_RHO__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "SCALE__1"
    Title: "SCALE__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SCALE__1"
    Title: "MUON_SCALE__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "MUON_SCALE__1"
    Title: "MUON_SCALE__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_MUON_SCALE__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "ATASF__1"
    Title: "ATASF__1"
    Type: HISTO
    Samples: Zprime
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1down"
    NtupleFilesUp: "sig_Zp072"
    NtupleFilesDown: "sig_Zp072"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "PRW_DATASF__1"
    Title: "PRW_DATASF__1"
    Type: HISTO
    Samples: ZZ4l
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1down"
    NtupleFilesUp: "bkg_qcd"
    NtupleFilesDown: "bkg_qcd"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

  Systematic: "PRW_DATASF__1"
    Title: "PRW_DATASF__1"
    Type: HISTO
    Samples: ggZZ
    Regions: ZZ4l_CR, Zprime_SR
    NtuplePathsUp: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1up"
    NtuplePathsDown: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_PRW_DATASF__1down"
    NtupleFilesUp: "bkg_ggZZ"
    NtupleFilesDown: "bkg_ggZZ"
    NtupleNameUp: "ntup"
    NtupleNameDown: "ntup"
    Symmetrisation: TWOSIDED
    WeightUp: weight
    WeightDown: weight

