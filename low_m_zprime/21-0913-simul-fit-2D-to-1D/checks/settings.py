from typing import Dict
import numpy as np

# parameters
fit_ntup_dir = "/data/zprime/ntuples_fit/21-0120-sys-best-limit"
# mass_points_high = ["42_mz2", 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75]
mass_points_high = [54, 57, 60, 63, 66, 69, 72, 75]
dnn_cut_dict = {
    54: 0.12,
    57: 0.12,
    60: 0.12,
    63: 0.14,
    66: 0.12,
    69: 0.16,
    72: 0.20,
    75: 0.14,
}

auto_bin_dict = {
    54: {},
    57: {},
    60: {},
    63: {},
    66: {},
    69: {},
    72: {},
    75: {},
}


def get_mass_cut(mass):
    sigma_range = 3.0 * (-0.0202966 + 0.0190822 * mass)
    return (mass - sigma_range, mass + sigma_range)


def get_bin_dict(mass):
    low, high = get_mass_cut(mass)
    bin_dict_SR = {}
    bin_dict_CR = {}

    # 1D case
    # 05 bins
    bin_dict_SR["05bins"], bin_dict_CR["05bins"] = get_nbin_edge(low, high, 5)
    # 10 bins
    bin_dict_SR["10bins"], bin_dict_CR["10bins"] = get_nbin_edge(low, high, 10)
    # 20 bins
    bin_dict_SR["20bins"], bin_dict_CR["20bins"] = get_nbin_edge(low, high, 20)

    # 2D case
    # 05 bins
    bin_dict_SR["2d_05bins"], bin_dict_CR["2d_05bins"] = get_nbin_edge_2d(low, high, 5)
    # 10 bins
    bin_dict_SR["2d_10bins"], bin_dict_CR["2d_10bins"] = get_nbin_edge_2d(low, high, 10)
    # 20 bins
    bin_dict_SR["2d_20bins"], bin_dict_CR["2d_20bins"] = get_nbin_edge_2d(low, high, 20)

    return bin_dict_SR, bin_dict_CR


def get_nbin_edge(low, high, bins, get_str=True):
    edges_sr = np.linspace(low, high, bins + 1)
    edges_cr_l = list(np.linspace(0, low, bins + 1))
    edges_cr_r = list(np.linspace(high, 80, bins + 1))
    edges_cr = edges_cr_l + edges_cr_r
    if get_str:
        sr_bin = ", ".join([str(x) for x in edges_sr])
        cr_bin = ", ".join([str(x) for x in edges_cr])
        return sr_bin, cr_bin
    else:
        return edges_sr, edges_cr


def get_nbin_edge_2d(low, high, bins, get_str=True, eps=0.01):
    low_2d, high_2d = int((low - 30) / 2), int((high - 30) / 2)
    edges_sr, edges_cr = [], []
    row_sr, row_cr = get_nbin_edge(low_2d, high_2d, bins, get_str=False)
    for row in range(low_2d, high_2d):
        edges_sr += [row * 50 + int(x) for x in row_sr]
        edges_cr += [row * 50 + int(x) for x in row_cr]
    edges_sr = list(set(edges_sr))
    edges_sr.sort()
    edges_cr = list(set(edges_cr))
    edges_cr.sort()
    if get_str:
        sr_bin = ", ".join([str(x) for x in edges_sr])
        cr_bin = ", ".join([str(x) for x in edges_cr])
        return sr_bin, cr_bin
    else:
        return edges_sr, edges_cr
