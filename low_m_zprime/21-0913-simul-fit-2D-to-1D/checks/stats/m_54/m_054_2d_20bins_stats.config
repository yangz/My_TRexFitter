% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "job_m_054_2d_20bins_stats"
  CmeLabel: "13 TeV"
  POI: "g"
  ReadFrom: NTUP
  NtuplePaths: "/data/zprime/ntuples_fit/21-0120-sys-best-limit/high_mass/tree_NOMINAL"
  Label: "Four Muon"
  LumiLabel: "140.0 fb^{-1}"    % data17?
  % Lumi: 140
  NtupleName: "ntup"
  DebugLevel: 0
  MCstatThreshold: 5%
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  

% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRSR
  FitType: SPLUSB
  POIAsimov: 0
  FitBlind: TRUE
  doLHscan: "g"

% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE
  SignalInjection: FALSE
  SignalInjectionValue: 1.0


% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: TRUE
  POIAsimov: 1


% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ZZ4l_CR"
  Type: CONTROL
  Selection: "((mz1 > 57.0304266) || (mz1 < 50.9695734) || (mz2 > 57.0304266) || (mz2 < 50.9695734)) && (dnn_out_sig > 0.12)"
  Variable: "int((mz1 - 30) / 2) * 50 + int((mz2 - 30) / 2)", 100, 0, 2500
  VariableTitle: "(mz1, mz2)"
  Binning: 500, 501, 502, 503, 504, 505, 506, 507, 508, 509, 510, 513, 516, 519, 523, 526, 529, 533, 536, 539, 543, 546, 549, 550, 551, 552, 553, 554, 555, 556, 557, 558, 559, 560, 563, 566, 569, 573, 576, 579, 580, 583, 586, 589, 593, 596, 599, 600, 601, 602, 603, 604, 605, 606, 607, 608, 609, 610, 613, 616, 619, 623, 626, 629, 630, 633, 636, 639, 643, 646, 649, 653, 656, 659, 663, 666, 669, 673, 676, 680
  Label: "Control Region"
  ShortLabel: "CR,4muon"

Region: "Zprime_SR"
  Type: SIGNAL
  Selection: "((mz1 > 50.9695734) && (mz1 < 57.0304266) && (mz2 > 50.9695734) && (mz2 < 57.0304266)) && (dnn_out_sig > 0.12)"
  Variable: "int((mz1 - 30) / 2) * 50 + int((mz2 - 30) / 2)", 100, 0, 2500
  VariableTitle: "(mz1, mz2)"
  Binning: 510, 511, 512, 513, 560, 561, 562, 563, 610, 611, 612, 613
  Label: "Signal Region"
  ShortLabel: "SR,4muon"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "Zprime"
  Type: SIGNAL
  Title: "Zprime"
  FillColor: 2
  LineColor: 2
  NtupleFile: "sig_Zp054"
  MCweight: "weight"

Sample: "ZZ4l"
  Type: BACKGROUND
  Title: "ZZ4l"
  FillColor: 4
  LineColor: 4
  NtupleFile: "bkg_qcd"
  MCweight: "weight"

Sample: "ggZZ"
  Type: BACKGROUND
  Title: "ggZZ"
  FillColor: 434
  LineColor: 434
  NtupleFile: "bkg_ggZZ"
  MCweight: "weight"

Sample: "fakes"
  Type: BACKGROUND
  Title: "fakes"
  FillColor: 880
  LineColor: 880
  NtupleFiles: "bkg_fakes_diboson","bkg_fakes_ttbar","bkg_fakes_zll"
  MCweight: "weight"

%Sample: "DD"
%  Type: BACKGROUND
%  Title: "DD"
%  FillColor: 434
%  LineColor: 434
%  NtupleFile: "tree_DD"
%  MCweight: "weight"
%  NormalizedByTheory: FALSE

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "g"
  Title: "#sigma(fb)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: Zprime

NormFactor: "muZZ4l"
  Title: "#mu (ZZ4l)"
  Min: 0
  Max: 10
  Nominal: 1
  % Constant: TRUE
  Samples: ZZ4l

% NormFactor: "muLumi"
%   Title: "Lumi Scale"
%   Min: 0
%   Max: 100
%   Nominal: 1
%   Constant: TRUE
%   Samples: all


% --------------- %
% - SYSTEMATICS - %
% --------------- %

%% Overall Systematics

  Systematic: "LUMI"
    Title: "Luminosity"
    Type: OVERALL
    Samples: all
    OverallUp: 0.021
    OverallDown: -0.021
    Category: Lumi

