p4_sys_names = [
    "FT_EFF_B_systematics__1",
    "FT_EFF_C_systematics__1",
    "FT_EFF_Light_systematics__1",
    "FT_EFF_extrapolation__1",
    "FT_EFF_extrapolation_from_charm__1",
    "MUON_EFF_ISO_STAT__1",
    "MUON_EFF_ISO_SYS__1",
    "MUON_EFF_RECO_STAT__1",
    "MUON_EFF_RECO_STAT_LOWPT__1",
    "MUON_EFF_RECO_SYS__1",
    "MUON_EFF_RECO_SYS_LOWPT__1",
    "MUON_EFF_TTVA_STAT__1",
    "MUON_EFF_TTVA_SYS__1",
    "MUON_ID__1",
    "MUON_MS__1",
    "MUON_SAGITTA_RESBIAS__1",
    "MUON_SAGITTA_RHO__1",
    "MUON_SCALE__1",
    "PRW_DATASF__1",
]

binning = {
    66: "860, 865, 866, 867, 868, 916, 917, 918, 919, 966, 967, 968, 969, 970",
    69: "915, 917, 918, 919, 967, 968, 969, 970, 1017, 1018, 1019, 1020, 1021, 1067, 1069, 1070",
    72: "1010, 1017, 1018, 1019, 1020, 1021, 1067, 1068, 1069, 1070, 1071, 1072, 1118, 1119, 1120, 1121, 1122, 1125",
    75: "1060, 1070, 1071, 1072, 1119, 1120, 1121, 1122, 1123, 1124, 1170, 1171, 1172, 1173, 1174, 1220, 1221, 1222, 1223, 1224, 1230",
}
