# Check TrexFitter Fitting with Fixed Scale

## Check Definition

|     Setup    | fit norm factor | fixed norm factor | apply uncertainty | cut DNN |
|:------------:|:---------------:|:-----------------:|:-----------------:|:-------:|
|      0-N     |        Y        |         -         |         -         |    N    |
|      1-N     |        -        |         Y         |         N         |    N    |
|      2-N     |        -        |         Y         |         Y         |    N    |
| 0-C(default) |        Y        |         -         |         -         |    Y    |
|      1-C     |        -        |         Y         |         N         |    Y    |
|      2-C     |        -        |         Y         |         Y         |    Y    |

Note: fixed norm factor calculated without DNN cut

## Results

Numbers are limit for **$BR\times XS$**:

|     Setup    | 15 GeV   | 35 GeV   | 51 GeV  | 66 GeV  |
|:------------:|----------|----------|---------|---------|
|      0-N     | 0.003425 | 0.008881 | 0.02808 | 0.08170 |
|      1-N     | 0.003415 | 0.008758 | 0.02768 | 0.08083 |
|      2-N     | 0.003419 | 0.008901 | 0.02827 | 0.08222 |
| 0-C(default) | 0.003296 | 0.008051 | 0.01505 | 0.07027 |
|      1-C     | 0.003243 | 0.007637 | 0.01574 | 0.07285 |
|      2-C     | 0.003352 | 0.008126 | 0.01536 | 0.07169 |
