% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "job_m_019_cut_p65_setup_1C"
  CmeLabel: "13 TeV"
  POI: "g"
  ReadFrom: NTUP
  NtuplePaths: "/data/zprime/ntuples_fit/22-0120-sys/low_mass/tree_NOMINAL"
  Label: "Four Muon"
  LumiLabel: "140.0 fb^{-1}"    % data17?
  % Lumi: 140
  NtupleName: "ntup"
  DebugLevel: 0
  MCstatThreshold: 5%
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  MergeUnderOverFlow: FALSE
  

% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRSR
  FitType: SPLUSB
  POIAsimov: 0
  FitBlind: FALSE
  doLHscan: "g"

% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: FALSE
  SignalInjection: FALSE
  SignalInjectionValue: 1.0


% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: FALSE
  POIAsimov: 1


% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ZZ4l_CR"
  Type: CONTROL
  Selection: "((mz2 > 20.020622319680637) || (mz2 < 17.979377680319363)) && (mz2 > 0) && (mz2 < 45) && (dnn_out_sig > 0.65)"
  Variable: "mz2 < 17.979377680319363 ? mz2 : (mz2 - 2.041244639361274)",10,0,42.958755360638726
  VariableTitle: "mz2"
  Binning: "AutoBin","TransfoD", 10, 10
  Label: "Control Region"
  ShortLabel: "CR,4muon"

Region: "Zprime_SR"
  Type: SIGNAL
  Selection: "((mz2 > 17.979377680319363) && (mz2 < 20.020622319680637)) && (dnn_out_sig > 0.65)"
  Variable: "mz2",10,17.979377680319363,20.020622319680637
  VariableTitle: "mz2"
  Binning: "AutoBin","TransfoD", 10, 10
  Label: "Signal Region"
  ShortLabel: "SR,4muon"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "Zprime"
  Type: SIGNAL
  Title: "Zprime"
  FillColor: 2
  LineColor: 2
  NtupleFile: "sig_Zp019"
  MCweight: "weight"

Sample: "ZZ4l"
  Type: BACKGROUND
  Title: "ZZ4l"
  FillColor: 4
  LineColor: 4
  NtupleFile: "bkg_qcd"
  MCweight: "weight"

Sample: "ggZZ"
  Type: BACKGROUND
  Title: "ggZZ"
  FillColor: 434
  LineColor: 434
  NtupleFile: "bkg_ggZZ"
  MCweight: "weight" * 1.7

Sample: "fakes_data"
  Type: BACKGROUND
  Title: "fakes_data"
  FillColorRGB: 244, 121, 66
  LineColorRGB: 0, 0, 0
  NtupleFiles: "bkg_fakes_data"
  MCweight: "weight"

Sample: "fakes_diboson"
  Type: BACKGROUND
  Title: "fakes_diboson"
  FillColor: 880
  LineColor: 880
  NtupleFiles: "bkg_fakes_diboson"
  MCweight: "weight"


Sample: "data"
  Type: DATA
  Title: "data"
  NtupleFile: "data_all"
  MCweight: "weight"
  NormalizedByTheory: FALSE

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "g"
  Title: "#sigma(fb)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: Zprime

NormFactor: "muZZ4l"
  Title: "#mu (ZZ4l)"
  Min: 0
  Max: 10
  Nominal: 1.0174
  Constant: TRUE
  Samples: ZZ4l, ggZZ, fakes_diboson

% NormFactor: "muLumi"
%   Title: "Lumi Scale"
%   Min: 0
%   Max: 100
%   Nominal: 1
%   Constant: TRUE
%   Samples: all


% --------------- %
% - SYSTEMATICS - %
% --------------- %

%% Overall Systematics

  Systematic: "LUMI"
    Title: "Luminosity"
    Type: OVERALL
    Samples: all
    OverallUp: 0.021
    OverallDown: -0.021
    Category: Lumi

  Systematic: "PS"
    Title: "Parton Shower"
    Type: OVERALL
    Samples: all
    OverallUp: 0.0189
    OverallDown: -0.0189
    Category: Theory

  Systematic: "Trigger_eff"
    Title: "Trigger eff"
    Type: OVERALL
    Samples: all
    OverallUp: 0.008
    OverallDown: -0.008
    Category: Trigger

  %Systematic: "muZZ4l"
  %  Title: "scale ZZ4l"
  %  Type: OVERALL
  %  Samples: all
  %  OverallUp: 0
  %  OverallDown: -0
  %  Category: muZZ4l

