import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import numpy as np
import ROOT
import xlrd
from settings import get_coup_limit

ampl.use_atlas_style(usetex=False)


# mass_low = [5, 7, 9, 11, 13, 15, 17, 19, 23, 27, 31, 35, 39]
mass_low = [27, 31, 35, 39]
#mass_low = []
# mass_high = [42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75]
mass_high = [42, 45, 48, 51]
#mass_high = []

# Default limits
workbook = xlrd.open_workbook("limits_unblind_2c.xls")
exp_limits_low = []
obs_limits_low = []
exp_limits_high = []
obs_limits_high = []
for i, m in enumerate(mass_low + mass_high):
    if m == 42:
        sheet = workbook.sheet_by_name(f"m_42_mz2")
    else:
        sheet = workbook.sheet_by_name(f"m_{m:02d}")
    if m < 42:
        exp_limits_low.append(sheet.col(0)[9].value)
        obs_limits_low.append(sheet.col(0)[12].value)
    else:
        exp_limits_high.append(sheet.col(0)[9].value)
        obs_limits_high.append(sheet.col(0)[12].value)

# Append extend limits
#for m in [42, 45, 48, 51]:
for m in [42, 45]:
    mass_low.append(m)
    limit_file = f"extend_fit/sys/m_{m}/job_m_0{m}_cut_p74_setup_2C/Limits/asymptotics/myLimit_CL95.root"
    root_file = ROOT.TFile.Open(limit_file)
    root_tree = root_file.stats
    for entry in root_tree:
        exp_limits_low.append(get_coup_limit(f"m_{m}", entry.exp_upperlimit))
        obs_limits_low.append(get_coup_limit(f"m_{m}", entry.obs_upperlimit))
        break
pre_exp = []
pre_obs = []
#for m in [27, 31, 35, 39]:
for m in [39]:
    limit_file = f"extend_fit/sys/m_{m}/job_m_0{m}_cut_p16_setup_2C/Limits/asymptotics/myLimit_CL95.root"
    root_file = ROOT.TFile.Open(limit_file)
    root_tree = root_file.stats
    for entry in root_tree:
        pre_exp.append(get_coup_limit(f"m_{m}", entry.exp_upperlimit))
        pre_obs.append(get_coup_limit(f"m_{m}", entry.obs_upperlimit))
        break
#mass_high = [27, 31, 35, 39] + mass_high
mass_high = [39] + mass_high
exp_limits_high = pre_exp + exp_limits_high
obs_limits_high = pre_obs + obs_limits_high

fig, ax = plt.subplots()
ax.plot(mass_low, exp_limits_low, "-rD", label="DNN_low, exp")
# ax.plot(mass_low, obs_limits_low, linestyle="-", label="DNN_low, obs")
ax.plot(mass_high, exp_limits_high, "-bD", label="DNN_high, exp")
ax.vlines(42, 0, 0.03)
# ax.plot(mass_high, obs_limits_high, linestyle="-", label="DNN_high, obs")

# Adjust plot
ax.set_xlabel(r"$m_{Z'}$ [GeV]")
ax.set_ylim(bottom=0)
ax.set_ylabel(r"$g_{Z'}$")
ax.legend(loc="lower right", facecolor="white", edgecolor="black", framealpha=1.0)
ampl.plot.draw_atlas_label(
    0.05, 0.95, ax=ax, status="internal", energy="13 TeV", lumi=139
)
# Save
fig.savefig("extend_limits.png")
fig.savefig("extend_limits.svg")
fig.savefig("extend_limits.pdf")
