import pathlib
import numpy as np
import atlas_mpl_style as ampl
import matplotlib.pyplot as plt
import ROOT
from matplotlib.ticker import LogLocator, NullFormatter

ampl.use_atlas_style()

exp_p = dict()
obs_p = dict()

# Retrieve p_values
for i, path in enumerate(sorted(pathlib.Path(".").rglob("**/*_2C/**/mySignificance_p0.root"))):
    try:
        mass = int(str(path.parts[1])[2:4])
        print("#" * 80, "mass =", mass)
        print(path)
        root_file = ROOT.TFile.Open(path.as_posix())
        root_tree = root_file.p0
        for entry in root_tree:
            exp_p[mass] = entry.med_pval
            print("med_pval:", entry.med_pval)
            obs_p[mass] = entry.obs_pval
            print("obs_pval:", entry.obs_pval)
    except:
        print("## invalid:", path)
        continue

# Get points
x = sorted(exp_p.keys())
y_exp = [exp_p[i] for i in x]
y_obs = [obs_p[i] for i in x]

# Get 0-5 significances
sig_ticks = dict()
for i in range(6):
    # significance to p-values
    sig_ticks[i] = 2*ROOT.Math.normal_cdf_c(i, 1)  # normal_cdf_c gives one-side p-value
# Make p_value plots
fig, ax = plt.subplots()
ax.plot(x, y_obs, c="red", label="Obs.")
#ax.plot(x, y_exp, c="black", linestyle="dashed", label="Exp.")
## plot 0-5 significances
ax2 = ax.twinx()
for i in range(6):
    ax.hlines(sig_ticks.values(), xmin=0, xmax=80, colors="black", linestyles="dotted")
## set axis
ax.set_xlim(0, 80)
ax.set_xlabel("$m_{\mu\mu} [GeV]$")
ax.set_ylabel("Local $p_{0}$")
ax.set_ylim(1e-7, 1e5)
ax.set_yscale("log")
ax2.set_ylim(1e-7, 1e5)
ax2.set_yscale("log")
## set ticks
y_major = LogLocator(base = 10.0, numticks = 20)
ax.yaxis.set_major_locator(y_major)
#ax2.yaxis.set_major_locator(y_major)
y_minor = LogLocator(base=10.0, subs = np.arange(1.0, 10.0) * 0.1, numticks=100)
ax.yaxis.set_minor_locator(y_minor)
#ax2.yaxis.set_minor_locator(y_minor)
#ax2.yaxis.set_major_formatter(NullFormatter())
#ax2.yaxis.set_minor_formatter(NullFormatter())
ax2.set_yticks(list(sig_ticks.values()), labels=[f"{x} $\sigma$" for x in sig_ticks.keys()])
## final modifications
#ax.legend(loc="upper right")
#ampl.draw_atlas_label(0.05, 0.95, ax=ax, status="int", energy="13 TeV", lumi=139, desc="Low mass $Z'$\n$4\mu$ channel")
ampl.draw_atlas_label(0.05, 0.95, ax=ax, status="int", energy="13 TeV", lumi=139)
## save plots
fig.savefig("p_values.png")
fig.savefig("p_values.pdf")
fig.savefig("p_values.eps")
