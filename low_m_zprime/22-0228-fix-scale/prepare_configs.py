import copy
import pathlib
from shutil import copyfile

from settings import *
from workspace_const import p4_sys_names

with open("config_template.config", "r") as f:
    config_temp = f.read()
with open("config_template_wt_sys_module.config", "r") as f:
    config_wt_sys_temp = f.read()
with open("config_template_p4_sys_module.config", "r") as f:
    config_p4_sys_temp = f.read()

for mass in mass_points_low + mass_points_high:
    if mass in mass_points_low:
        region = "low_mass"
        fit_var = "mz2"
        p_cr_low = 0
        p_cr_high = 45
    elif mass in mass_points_high:
        region = "high_mass"
        fit_var = "mz1"
        p_cr_low = 30
        p_cr_high = 85
    else:
        print("#### ERROR! Unknown mass!")
        exit(1)
    dnn = dnn_cut_dict[mass]
    sm_scale = sm_scale_dict[mass]
    sm_scale_err = sm_scale_err_dict[mass]
    if mass == "42_mz2":
        mass_str = "m_42_mz2"
        fit_var = "mz2"
        mass = 42
    else:
        mass_str = f"m_{mass:02d}"

    config_para_base = {
        "p_mass": mass,
        "p_fit_var": fit_var,
        "p_ntuple_path": f"{fit_ntup_dir}/{region}/tree_NOMINAL",
        "p_region": region,
        "p_cr_cut_low": p_cr_low,
        "p_cr_cut_high": p_cr_high,
        "p_cr_bin": 10,
        "p_sr_bin": 10,
    }

    # for dnn_cut in [0, dnn_cut_dict[mass]]:
    for setup in range(6):
        config_para = copy.deepcopy(config_para_base)
        setup_id = setup >> 1
        setup_type = setup & 1

        dnn_cut = dnn if setup_type else 0
        dnn_label = f"p{int(dnn_cut*100):02d}"
        config_para["p_dnn_cut"] = dnn_cut
        config_para["p_dnn_label"] = dnn_label

        if setup_id == 0:
            config_para["p_constant_scale"] = "FALSE"
            config_para["p_fixed_scale"] = 1
            config_para["p_fixed_scale_uncert"] = 0
            config_para["p_comment_uncert"] = "%"
        elif setup_id == 1:
            config_para["p_constant_scale"] = "TRUE"
            config_para["p_fixed_scale"] = sm_scale
            config_para["p_fixed_scale_uncert"] = 0
            config_para["p_comment_uncert"] = "%"
        else:
            config_para["p_constant_scale"] = "TRUE"
            config_para["p_fixed_scale"] = sm_scale
            config_para["p_fixed_scale_uncert"] = sm_scale_err
            config_para["p_comment_uncert"] = ""

        setup_name = f"{setup_id}C" if setup_type else f"{setup_id}N"
        job_name = f"m_{mass:03d}_cut_{dnn_label}_setup_{setup_name}"
        config_para["p_job_name"] = job_name

        # prepare folders
        stats_dir = pathlib.Path(f"stats/{mass_str}")
        stats_dir.mkdir(parents=True, exist_ok=True)
        sys_dir = pathlib.Path(f"sys/{mass_str}")
        sys_dir.mkdir(parents=True, exist_ok=True)
        m_low, m_high = get_mass_cut(mass)
        bin_sr, bin_cr = get_bin_dict(mass)
        config_para["p_mass_cut_low"] = m_low
        config_para["p_mass_cut_high"] = m_high
        config_para["p_window"] = m_high - m_low
        config_para["p_cr_low"] = p_cr_low
        config_para["p_cr_high"] = p_cr_high - (m_high - m_low)

        for ky, sr_bins in bin_sr.items():
            cr_bins = bin_cr[ky]
            config_para["p_binning_cr"] = cr_bins
            config_para["p_binning_sr"] = sr_bins
            # write config without sys
            new_config_stats = config_temp.format(**config_para)
            config_name = f"m_{job_name}.config"
            with stats_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
                f.write(new_config_stats)

            # prepare sys config
            # set nominal config
            new_config = config_temp.format(**config_para)

            # add weight systematic config
            sys_config = ""
            config_wt_config = config_wt_sys_temp.format(
                p_mass=mass,
                p_ntuple_path=f"{fit_ntup_dir}/{region}/tree_NOMINAL",
                p_region=region,
                p_qcd_var=fit_var,
            )
            sys_config += config_wt_config
            # add p4 systematic config
            for sys_name in p4_sys_names:
                ntuple_path_up_sig = f"{fit_ntup_dir}/{region}/tree_{sys_name}up"
                ntuple_path_down_sig = f"{fit_ntup_dir}/{region}/tree_{sys_name}down"
                # sig
                sys_entry = config_p4_sys_temp.format(
                    p_sys_name=sys_name[5:],
                    p_sample="Zprime",
                    p_ntuple_path_up=ntuple_path_up_sig,
                    p_ntuple_path_down=ntuple_path_down_sig,
                    p_ntuple_files=f"sig_Zp{mass:03d}",
                    p_weight_str="weight",
                )
                sys_config += sys_entry
                # bkg
                ntuple_path_up_bkg = f"{fit_ntup_dir}/{region}/tree_{sys_name}up"
                ntuple_path_down_bkg = f"{fit_ntup_dir}/{region}/tree_{sys_name}down"
                ## add qcd
                sys_entry = config_p4_sys_temp.format(
                    p_sys_name=sys_name,
                    p_sample="ZZ4l",
                    p_ntuple_path_up=ntuple_path_up_bkg,
                    p_ntuple_path_down=ntuple_path_down_bkg,
                    p_ntuple_files="bkg_qcd",
                    p_weight_str="weight",
                )
                sys_config += sys_entry
                ## add ggZZ
                sys_entry = config_p4_sys_temp.format(
                    p_sys_name=sys_name,
                    p_sample="ggZZ",
                    p_ntuple_path_up=ntuple_path_up_bkg,
                    p_ntuple_path_down=ntuple_path_down_bkg,
                    p_ntuple_files="bkg_ggZZ",
                    p_weight_str="weight",
                )
                sys_config += sys_entry
            new_config += sys_config
            # write config with sys
            config_name = f"m_{job_name}.config"
            with sys_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
                f.write(new_config)

    fit_script_name = "fit_all.sh"
    copyfile(f"./{fit_script_name}", stats_dir.joinpath(fit_script_name))
    copyfile(f"./{fit_script_name}", sys_dir.joinpath(fit_script_name))

fit_script_name = "get_limits.py"
copyfile(f"./{fit_script_name}", pathlib.Path(f"stats").joinpath(fit_script_name))
copyfile(f"./{fit_script_name}", pathlib.Path(f"sys").joinpath(fit_script_name))
