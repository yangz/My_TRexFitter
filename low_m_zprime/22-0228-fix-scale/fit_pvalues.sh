#!/bin/bash

cd sys

find . -maxdepth 1 -type d -name "m*" -print0 | while read -d $'\0' file; do
    cd $file
    pwd
    find . -name "*.config" -print0 | while read -d $'\0' file; do
        echo $file
        trex-fitter s ./$file >/dev/null 2>&1 &
    done
    cd ..
done

cd ..
