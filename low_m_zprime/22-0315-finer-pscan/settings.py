import numpy as np

# parameters
fit_ntup_dir = "/data/zprime/ntuples_fit/22-0120-sys"
mass_points_low = np.array([5, 7, 9, 11, 13, 15, 17, 19, 23, 27, 31, 35, 39, 42])
#mass_points_high = ["42_mz2", 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75]
mass_points_high = np.array([42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75])

dnn_cut_low_dict = {
    5: 0.46,
    7: 0.58,
    9: 0.50,
    11: 0.60,
    13: 0.52,
    15: 0.54,
    17: 0.54,
    19: 0.65,
    23: 0.54,
    27: 0.65,
    31: 0.68,
    35: 0.72,
    39: 0.74,
    42: 0.74,
}

dnn_cut_high_dict = {
    42: 0.16,
    45: 0.10,
    48: 0.10,
    51: 0.14,
    54: 0.12,
    57: 0.12,
    60: 0.12,
    63: 0.14,
    66: 0.12,
    69: 0.16,
    72: 0.20,
    75: 0.14,
}

sm_scale_dict = {
    5: 1.01416,
    7: 1.02354,
    9: 1.01748,
    11: 1.02511,
    13: 1.016,
    15: 1.02193,
    17: 1.01605,
    19: 1.0174,
    23: 1.01802,
    27: 1.00328,
    31: 1.01295,
    35: 1.00904,
    39: 0.994415,
    "42_mz2": 1.14275,
    42: 0.904591,
    45: 0.937469,
    48: 0.939939,
    51: 0.940637,
    54: 0.951585,
    57: 0.949673,
    60: 0.95538,
    63: 0.94941,
    66: 0.949958,
    69: 0.955026,
    72: 0.953898,
    75: 0.953514,
}


sm_scale_err_dict = {
    5: 0.076795,
    7: 0.0770368,
    9: 0.0771363,
    11: 0.0769975,
    13: 0.0769927,
    15: 0.0768998,
    17: 0.0767256,
    19: 0.077677,
    23: 0.0783057,
    27: 0.0777129,
    31: 0.0783541,
    35: 0.0784479,
    39: 0.0789799,
    "42_mz2": 0.113996,
    42: 0.0746982,
    45: 0.075661,
    48: 0.0760602,
    51: 0.076524,
    54: 0.0774542,
    57: 0.0777304,
    60: 0.07497,
    63: 0.0744933,
    66: 0.0745495,
    69: 0.0750354,
    72: 0.0748922,
    75: 0.0747824,
}


def get_mass_cut(mass):
    # sigma_range = 3.0 * (0.0401444 + 0.0122949 * mass + 0.000140695 * mass * mass)  # @ Bing Li
    # sigma_range = 3.0 * (0.193 - 0.002 * mass + 0.00035 * mass * mass)  # @ Shuzhou Zhang
    sigma = np.poly1d(
        [
            5.20753537e-08,
            3.34322964e-08,
            -2.17314377e-04,
            2.26208490e-02,
            -1.81540253e-02,
        ]
    )
    sigma_range = 3 * sigma(mass)  # @ Shuzhou Update 2022/01/24
    return (mass - sigma_range, mass + sigma_range)


def get_bin_dict(mass):
    bin_dict_SR = {}
    bin_dict_CR = {}

    # 1D case
    # 10 bins
    #bin_dict_SR["10bins"], bin_dict_CR["10bins"] = 10, 10
    # 20 bins
    #bin_dict_SR["20bins"], bin_dict_CR["20bins"] = 20, 20

    # 10 bins
    #bin_dict_SR["10bins_auto"] = 'Binning: "AutoBin","TransfoD", 5, 5'
    #bin_dict_CR["10bins_auto"] = 'Binning: "AutoBin","TransfoD", 5, 5'
    # 20 bins
    bin_dict_SR["20bins_auto"] = 'Binning: "AutoBin","TransfoD", 10, 10'
    bin_dict_CR["20bins_auto"] = 'Binning: "AutoBin","TransfoD", 10, 10'

    return bin_dict_SR, bin_dict_CR


def get_coup_limit(mass_name, x):
    para_dict = {
        "m_05": [
            0.00014285959775009547,
            0.0030375747822960638,
            -0.0009267506934122755,
            0.0001709391854261289,
            -1.520500429564142e-05,
            5.129588006241013e-07,
        ],
        "m_07": [
            0.00017595453584282666,
            0.004506350002712834,
            -0.0019296193555482387,
            0.0005014119356643884,
            -6.288800567329947e-05,
            2.9924218006655305e-06,
        ],
        "m_09": [
            0.0002054556588638739,
            0.005968102987335838,
            -0.003208684966918542,
            0.0010498981243397564,
            -0.00016593297391993023,
            9.952291054296552e-06,
        ],
        "m_11": [
            0.00023434955540410396,
            0.007533804903872736,
            -0.004859314956131867,
            0.0019123048399373303,
            -0.000363726434025221,
            2.6260005862293543e-05,
        ],
        "m_13": [
            0.00026372453497646104,
            0.00927221893943123,
            -0.007012556831871669,
            0.0032431471878829783,
            -0.0007253313488181435,
            6.158957856185753e-05,
        ],
        "m_15": [
            0.00030140131268462095,
            0.009928707323140365,
            -0.0066873563929840596,
            0.0027497376729996767,
            -0.0005465517023259065,
            4.123766016733218e-05,
        ],
        "m_17": [
            0.00034102924027640464,
            0.010778930231433228,
            -0.006745991587815559,
            0.0025747926073592577,
            -0.0004749596227248707,
            3.325787992210886e-05,
        ],
        "m_19": [
            0.0003829175036602703,
            0.01182948238296452,
            -0.007104051922965414,
            0.002600374092165853,
            -0.00045994037888638515,
            3.087762082618056e-05,
        ],
        "m_23": [
            0.0004897939381709948,
            0.012330725436757579,
            -0.00511527756530375,
            0.0012872200675510146,
            -0.00015632526205605987,
            7.20212113738297e-06,
        ],
        "m_27": [
            0.0006125885457362561,
            0.014177661834207189,
            -0.005057743806702551,
            0.0010925494700167217,
            -0.00011385855228070985,
            4.501105148540806e-06,
        ],
        "m_31": [
            0.0007565984105016778,
            0.017291151086781597,
            -0.006016140173792976,
            0.0012671067367092646,
            -0.0001287343966321599,
            4.96102441157784e-06,
        ],
        "m_35": [
            0.0009613221073674271,
            0.01782606670819882,
            -0.0042425676872327065,
            0.0006089171088035651,
            -4.211651976156558e-05,
            1.1045069428457266e-06,
        ],
        "m_39": [
            0.0012016009128179765,
            0.020711219989254324,
            -0.004287988552453745,
            0.0005346100194725886,
            -3.2107742086009534e-05,
            7.310223187075262e-07,
        ],
        "m_42": [
            0.001413639665734382,
            0.025661920212525682,
            -0.005859087350315529,
            0.0008064572530198843,
            -5.34920003945024e-05,
            1.3453290734789734e-06,
        ],
        "m_42_mz2": [
            0.001413639665734382,
            0.025661920212525682,
            -0.005859087350315529,
            0.0008064572530198843,
            -5.34920003945024e-05,
            1.3453290734789734e-06,
        ],
        "m_45": [
            0.0016642885386911354,
            0.03242981601260141,
            -0.008434036788708524,
            0.0013240818801412155,
            -0.00010020248969094798,
            2.8754708886295387e-06,
        ],
        "m_48": [
            0.001966551519235829,
            0.0417826831724651,
            -0.012756596725438874,
            0.002354051096270498,
            -0.00020945475864435754,
            7.068112344262865e-06,
        ],
        "m_51": [
            0.0023224160441653713,
            0.0548259786981602,
            -0.020176464093835427,
            0.0044964723204448526,
            -0.00048341538776611675,
            1.9714058629911877e-05,
        ],
        "m_54": [
            0.002847330784353659,
            0.06015782161296643,
            -0.01817079334614929,
            0.003315620739819685,
            -0.0002916616710874115,
            9.729431862949621e-06,
        ],
        "m_57": [
            0.0034543530094960726,
            0.0707360500652855,
            -0.02008497570213265,
            0.00344342948855043,
            -0.00028454653591703807,
            8.915809182644659e-06,
        ],
        "m_60": [
            0.00424375697336179,
            0.07870359839248324,
            -0.018596062522632132,
            0.002648271366294158,
            -0.00018170308812117926,
            4.72598956012328e-06,
        ],
        "m_63": [
            0.005183227768423572,
            0.09311311917943178,
            -0.020787111897707888,
            0.002795867443978309,
            -0.00018125064871498723,
            4.454404106768783e-06,
        ],
        "m_66": [
            0.006256437629747566,
            0.11337448540561064,
            -0.02555348300915392,
            0.003463804281430823,
            -0.00022623427092564134,
            5.598118051902577e-06,
        ],
        "m_69": [
            0.007437009700996319,
            0.13871544818408635,
            -0.03256666852093385,
            0.004587420176565141,
            -0.0003113237247549025,
            7.997652032141912e-06,
        ],
        "m_72": [
            0.008670962916577311,
            0.16656136940447963,
            -0.04063251016443493,
            0.005931787980697107,
            -0.0004175042062685075,
            1.1116761369568317e-05,
        ],
        "m_75": [
            0.009854199243512225,
            0.1942626498410473,
            -0.04854993911222515,
            0.007224562470082335,
            -0.0005178731029686154,
            1.4011994863249172e-05,
        ],
        "m_10": [
            0.0002227282421805712,
            0.006264483507441368,
            -0.0031781875545881964,
            0.0009805232453748824,
            -0.00014609179976095968,
            8.259938550199512e-06,
        ],
    }

    p = para_dict[mass_name]
    return (
        p[0] + p[1] * x + p[2] * x ** 2 + p[3] * x ** 3 + p[4] * x ** 4 + p[5] * x ** 5
    )

