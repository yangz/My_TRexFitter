% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "job_{p_job_name}"
  CmeLabel: "13 TeV"
  POI: "g"
  ReadFrom: NTUP
  NtuplePaths: "{p_ntuple_path}"
  Label: "Four Muon"
  LumiLabel: "140.0 fb^{{-1}}"    % data17?
  % Lumi: 140
  NtupleName: "ntup"
  DebugLevel: 0
  MCstatThreshold: 5%
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  MergeUnderOverFlow: FALSE
  

% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRSR
  FitType: SPLUSB
  POIAsimov: 0
  FitBlind: FALSE
  doLHscan: "g"

% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: FALSE
  SignalInjection: FALSE
  SignalInjectionValue: 1.0


% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: FALSE
  POIAsimov: 1


% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ZZ4l_CR"
  Type: CONTROL
  Selection: "(({p_fit_var} > {p_mass_cut_high}) || ({p_fit_var} < {p_mass_cut_low})) && ({p_fit_var} > {p_cr_cut_low}) && ({p_fit_var} < {p_cr_cut_high}) && (dnn_out_sig > {p_dnn_cut})"
  Variable: "{p_fit_var} < {p_mass_cut_low} ? {p_fit_var} : ({p_fit_var} - {p_window})",{p_cr_bin},{p_cr_low},{p_cr_high}
  VariableTitle: "{p_fit_var}"
  {p_binning_cr}
  Label: "Control Region"
  ShortLabel: "CR,4muon"

Region: "Zprime_SR"
  Type: SIGNAL
  Selection: "(({p_fit_var} > {p_mass_cut_low}) && ({p_fit_var} < {p_mass_cut_high})) && (dnn_out_sig > {p_dnn_cut})"
  Variable: "{p_fit_var}",{p_sr_bin},{p_mass_cut_low},{p_mass_cut_high}
  VariableTitle: "{p_fit_var}"
  {p_binning_sr}
  Label: "Signal Region"
  ShortLabel: "SR,4muon"

% --------------- %
% --- SAMPLES --- %
% --------------- %

Sample: "Zprime"
  Type: SIGNAL
  Title: "Zprime"
  FillColor: 2
  LineColor: 2
  NtupleFile: "sig_Zp{p_mass:03d}"
  MCweight: "weight"

Sample: "ZZ4l"
  Type: BACKGROUND
  Title: "ZZ4l"
  FillColor: 4
  LineColor: 4
  NtupleFile: "bkg_qcd"
  MCweight: "weight"

Sample: "ggZZ"
  Type: BACKGROUND
  Title: "ggZZ"
  FillColor: 434
  LineColor: 434
  NtupleFile: "bkg_ggZZ"
  MCweight: "weight" * 1.7

Sample: "fakes_data"
  Type: BACKGROUND
  Title: "fakes_data"
  FillColorRGB: 244, 121, 66
  LineColorRGB: 0, 0, 0
  NtupleFiles: "bkg_fakes_data"
  MCweight: "weight"

Sample: "fakes_diboson"
  Type: BACKGROUND
  Title: "fakes_diboson"
  FillColor: 880
  LineColor: 880
  NtupleFiles: "bkg_fakes_diboson"
  MCweight: "weight"


Sample: "data"
  Type: DATA
  Title: "data"
  NtupleFile: "data_all"
  MCweight: "weight"
  NormalizedByTheory: FALSE

% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "g"
  Title: "#sigma(fb)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: Zprime

NormFactor: "muZZ4l"
  Title: "#mu (ZZ4l)"
  Min: 0
  Max: 10
  Nominal: {p_fixed_scale}
  Constant: {p_constant_scale}
  Samples: ZZ4l, ggZZ, fakes_diboson

% NormFactor: "muLumi"
%   Title: "Lumi Scale"
%   Min: 0
%   Max: 100
%   Nominal: 1
%   Constant: TRUE
%   Samples: all


% --------------- %
% - SYSTEMATICS - %
% --------------- %

%% Overall Systematics

  Systematic: "LUMI"
    Title: "Luminosity"
    Type: OVERALL
    Samples: all
    OverallUp: 0.021
    OverallDown: -0.021
    Category: Lumi

  Systematic: "PS"
    Title: "Parton Shower"
    Type: OVERALL
    Samples: all
    OverallUp: 0.0189
    OverallDown: -0.0189
    Category: Theory

  Systematic: "Trigger_eff"
    Title: "Trigger eff"
    Type: OVERALL
    Samples: all
    OverallUp: 0.008
    OverallDown: -0.008
    Category: Trigger

  {p_comment_uncert}Systematic: "muZZ4l"
  {p_comment_uncert}  Title: "scale ZZ4l"
  {p_comment_uncert}  Type: OVERALL
  {p_comment_uncert}  Samples: all
  {p_comment_uncert}  OverallUp: {p_fixed_scale_uncert}
  {p_comment_uncert}  OverallDown: -{p_fixed_scale_uncert}
  {p_comment_uncert}  Category: muZZ4l

