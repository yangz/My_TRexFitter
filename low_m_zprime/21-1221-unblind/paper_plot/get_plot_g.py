# estimate the g values for the plotting scale

# inputs
events_15 = 99.27
events_51 = 24.21

# copy xs to coup function from "get_limits.py"
def get_coup_limit(mass_name, x):
    para_dict = {
        "m_15": [
            0.00030140131268462095,
            0.009928707323140365,
            -0.0066873563929840596,
            0.0027497376729996767,
            -0.0005465517023259065,
            4.123766016733218e-05,
        ],
        "m_51": [
            0.0023224160441653713,
            0.0548259786981602,
            -0.020176464093835427,
            0.0044964723204448526,
            -0.00048341538776611675,
            1.9714058629911877e-05,
        ],
    }

    p = para_dict[mass_name]
    return (
        p[0] + p[1] * x + p[2] * x ** 2 + p[3] * x ** 3 + p[4] * x ** 4 + p[5] * x ** 5
    )


# calculate g
xs_15 = events_15 / (139 * 0.1889 * 0.98 * (1/3))
g_15 = get_coup_limit("m_15", xs_15)
print(f"mass=15 GeV, xs={xs_15}, g={g_15}")

xs_51 = events_51 / (139 * 0.1889 * 0.98 * (1/3))
g_51 = get_coup_limit("m_51", xs_51)
print(f"mass=51 GeV, xs={xs_51}, g={g_51}")

# infomation
"""
# equation
n_events = lumi x XS*BR x eff_cut x eff_DNN
lumi = 139 fb^{-1}
BR = 1/3
XS = n_events / (lumi x eff_cut x eff_DNN x (1/3))

# cut eff (from email exchange 2022-01-01):
mass 5 GeV, eff:  0.0940840897715409
mass 7 GeV, eff:  0.11283023031664079
mass 9 GeV, eff:  0.12681518518503113
mass 11 GeV, eff:  0.145752069891012
mass 13 GeV, eff:  0.16537386404336724
mass 15 GeV, eff:  0.18893425118148166  <--
mass 17 GeV, eff:  0.2055061626242246
mass 19 GeV, eff:  0.20884906598925665
mass 23 GeV, eff:  0.22021188693242924
mass 27 GeV, eff:  0.22228516195453427
mass 31 GeV, eff:  0.2173316861924262
mass 35 GeV, eff:  0.19075222230765462
mass 39 GeV, eff:  0.1605327194770645
mass 42 GeV, eff:  0.1812207530767357
mass 45 GeV, eff:  0.2550610115368609
mass 48 GeV, eff:  0.2948755758846919
mass 51 GeV, eff:  0.3093456925085074  <--
mass 54 GeV, eff:  0.3036835541313455
mass 57 GeV, eff:  0.2910733570008095
mass 60 GeV, eff:  0.2588836383798135
mass 63 GeV, eff:  0.22319114993610148
mass 66 GeV, eff:  0.1873513552239558
mass 69 GeV, eff:  0.15095354075390255
mass 72 GeV, eff:  0.12684728557317576
mass 75 GeV, eff:  0.10260159942258203

# DNN sig eff :
mass 5 GeV, eff:  98.0 %
mass 7 GeV, eff:  98.2 %
mass 9 GeV, eff:  98.3 %
mass 11 GeV, eff: 98.2 %
mass 13 GeV, eff: 98.0 %
mass 15 GeV, eff: 98.0 %  <--
mass 17 GeV, eff: 98.2 %
mass 19 GeV, eff: 98.0 %
mass 23 GeV, eff: 97.8 %
mass 27 GeV, eff: 97.6 %
mass 31 GeV, eff: 97.0 %
mass 35 GeV, eff: 96.8 %
mass 39 GeV, eff: 95.2 %
mass 42 GeV, eff: 88.0 %
mass 45 GeV, eff: 88.6 %
mass 48 GeV, eff: 88.9 %
mass 51 GeV, eff: 79.2 %  <--
mass 54 GeV, eff: 78.7 %
mass 57 GeV, eff: 73.1 %
mass 60 GeV, eff: 68.0 %
mass 63 GeV, eff: 62.5 %
mass 66 GeV, eff: 52.5 %
mass 69 GeV, eff: 46.3 %
mass 72 GeV, eff: 51.2 %
mass 75 GeV, eff: 67.9 %

"""