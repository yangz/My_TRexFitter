#!/bin/bash

find . -maxdepth 1 -type d -name "m*" -print0 | while read -d $'\0' file; do
    cd $file
    pwd
    trex-fitter s *.config > /dev/null 2>&1 &
    cd ..
done
