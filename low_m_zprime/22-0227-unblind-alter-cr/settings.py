import numpy as np

# parameters
fit_ntup_dir = "/data/zprime/ntuples_fit/21-0120-sys-best-limit"
mass_points_low = [5, 7, 9, 11, 13, 15, 17, 19, 23, 27, 31, 35, 39]
mass_points_high = ["42_mz2", 42, 45, 48, 51, 54, 57, 60, 63, 66, 69, 72, 75]
# mass_points_high = [54, 57, 60, 63, 66, 69, 72, 75]

dnn_cut_dict = {
    5: 0.46,
    7: 0.58,
    9: 0.50,
    11: 0.60,
    13: 0.52,
    15: 0.54,
    17: 0.54,
    19: 0.65,
    23: 0.54,
    27: 0.65,
    31: 0.68,
    35: 0.72,
    39: 0.74,
    "42_mz2": 0.24,
    42: 0.16,
    45: 0.10,
    48: 0.10,
    51: 0.14,
    54: 0.12,
    57: 0.12,
    60: 0.12,
    63: 0.14,
    66: 0.12,
    69: 0.16,
    72: 0.20,
    75: 0.14,
}


def get_mass_cut(mass):
    # sigma_range = 3.0 * (0.0401444 + 0.0122949 * mass + 0.000140695 * mass * mass)  # @ Bing Li
    # sigma_range = 3.0 * (0.193 - 0.002 * mass + 0.00035 * mass * mass)  # @ Shuzhou Zhang
    sigma = np.poly1d(
        [
            5.20753537e-08,
            3.34322964e-08,
            -2.17314377e-04,
            2.26208490e-02,
            -1.81540253e-02,
        ]
    )
    sigma_range = 3 * sigma(mass)  # @ Shuzhou Update 2022/01/24
    return (mass - sigma_range, mass + sigma_range)


def get_bin_dict(mass):
    bin_dict_SR = {}
    bin_dict_CR = {}
    # 20 bins
    bin_dict_SR["20bins_auto"] = 'Binning: "AutoBin","TransfoD", 10, 10'
    bin_dict_CR["20bins_auto"] = 'Binning: "AutoBin","TransfoD", 10, 10'

    return bin_dict_SR, bin_dict_CR