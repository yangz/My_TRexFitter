# Internal Checks on Unblind Results

## settings

* mass window: 3 sigma with **UPDATED** quad fit method
* **[new]** control region: use alternative control region definition
  * high mass: use full range of $m_{Z2}$ as CR
  * low mass: use full range of $m_{Z1}$ as CR

## Plots

* p_values:

    ```bash
    cd sys
    conda activate pyroot
    source fit_pvalues.py
    python get_pvalues.py
    ```
