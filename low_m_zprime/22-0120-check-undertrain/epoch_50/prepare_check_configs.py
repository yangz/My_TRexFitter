import pathlib
from shutil import copyfile

import numpy as np
from settings import *

with open("config_template.config", "r") as f:
    config_temp = f.read()
with open("config_template_wt_sys_module.config", "r") as f:
    config_wt_sys_temp = f.read()
with open("config_template_p4_sys_module.config", "r") as f:
    config_p4_sys_temp = f.read()

dnn_cuts_high = np.linspace(0, 0.5, 26)
dnn_cuts_low = np.linspace(0.3, 0.8, 26)

#for mass in mass_points_low + mass_points_high:
for mass in mass_points_high:
    if mass in mass_points_low:
        region = "low_mass"
        fit_var = "mz2"
        p_cr_low=0
        p_cr_high=80
        dnn_cuts = dnn_cuts_low
    elif mass in mass_points_high:
        region = "high_mass"
        fit_var = "mz1"
        p_cr_low=30
        p_cr_high=120
        dnn_cuts = dnn_cuts_high
    else:
        print("#### ERROR! Unknown mass!")
        exit(1)
    if mass == "42_mz2":
        mass_str = "m_42_mz2"
        fit_var = "mz2"
        mass = 42
    else:
        mass_str = f"m_{mass:02d}"

    # prepare folders
    stats_dir = pathlib.Path(f"stats/{mass_str}")
    stats_dir.mkdir(parents=True, exist_ok=True)
    sys_dir = pathlib.Path(f"sys/{mass_str}")
    sys_dir.mkdir(parents=True, exist_ok=True)
    m_low, m_high = get_mass_cut(mass)
    bin_sr, bin_cr = get_bin_dict(mass)
    for ky, sr_bins in bin_sr.items():
        cr_bins = bin_cr[ky]
        for dnn_cut in dnn_cuts:
            dnn_label = f"{int(dnn_cut*100):02d}"
            # write config without sys
            if "auto" in ky:
                new_config_stats = config_temp.format(
                    p_job=ky,
                    p_fit_type="stats",
                    p_mass=mass,
                    p_fit_var=fit_var,
                    p_ntuple_path=f"{fit_ntup_dir}/{region}",
                    p_mass_cut_low=m_low,
                    p_mass_cut_high=m_high,
                    p_dnn_cut=dnn_cut,
                    p_dnn_cut_label=dnn_label,
                    p_region=region,
                    p_window=m_high - m_low,
                    p_cr_low=p_cr_low,
                    p_cr_high=p_cr_high - (m_high - m_low),
                    p_cr_bin=10,
                    p_sr_bin=10,
                    p_binning_cr=cr_bins,
                    p_binning_sr=sr_bins,
                )
            else:
                new_config_stats = config_temp.format(
                    p_job=ky,
                    p_fit_type="stats",
                    p_mass=mass,
                    p_fit_var=fit_var,
                    p_ntuple_path=f"{fit_ntup_dir}/{region}",
                    p_mass_cut_low=m_low,
                    p_mass_cut_high=m_high,
                    p_dnn_cut=dnn_cut,
                    p_dnn_cut_label=dnn_label,
                    p_region=region,
                    p_window=m_high - m_low,
                    p_cr_low=p_cr_low,
                    p_cr_high=p_cr_high - (m_high - m_low),
                    p_cr_bin=cr_bins,
                    p_sr_bin=sr_bins,
                    p_binning_cr="",
                    p_binning_sr="",
                )
            config_name = f"m_{mass:03d}_{ky}_p{dnn_label}_stats.config"
            with stats_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
                f.write(new_config_stats)

    fit_script_name = "fit_all.sh"
    copyfile(f"./{fit_script_name}", stats_dir.joinpath(fit_script_name))
    copyfile(f"./{fit_script_name}", sys_dir.joinpath(fit_script_name))

fit_script_name = "get_limits.py"
copyfile(f"./{fit_script_name}", pathlib.Path(f"stats").joinpath(fit_script_name))
copyfile(f"./{fit_script_name}", pathlib.Path(f"sys").joinpath(fit_script_name))
