import pathlib
from shutil import copyfile

# Constants
mass_points = [300, 400]
scales = [0.01, 0.1, 1, 10, 100, 1000, 10000]

# Create config files
with open("template.config", "r") as f:
    config_temp = f.read()
for m in mass_points:
    # prepare folders
    m_dir = pathlib.Path(f"stats/m_{m:04d}")
    m_dir.mkdir(parents=True, exist_ok=True)
    # add config for each injection
    for x in scales:
        content = config_temp.format(p_MASS=m, p_scale=x)
        config_name = f"m_{m:04d}_scale_{x}_stats.config"
        with m_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
            f.write(content)
    # copy helper scripts
    copyfile(f"./fit_all.sh", m_dir.joinpath("fit_all.sh"))
