import pandas as pd
import matplotlib.pylab as plt
import numpy as np
import uproot

ntup_path = "/net/s3_datac/shuzhou/V09/2tag_sr1_applied/sig_res_sysweight_alter.root"

# Get Data
df: pd.DataFrame = uproot.concatenate(
    f"{ntup_path}:Nominal", ["m_hh", "m_hh_truth", "weight"], library="pd"
)
# ll_m_251 = df.loc[df["m_hh_truth"] == 251, "ll_m"].to_numpy()
# wt_251 = df.loc[df["m_hh_truth"] == 251, "ll_m"].to_numpy()
# print(np.dot(ll_m_251, wt_251))  # DEBUG

# Plot mass peak
masses = [251, 260, 300, 400, 500, 600, 800, 1000]
fig, ax = plt.subplots()
for m in masses:
    ll_m = df.loc[df["m_hh_truth"] == m, "m_hh"].to_numpy()
    wt = df.loc[df["m_hh_truth"] == m, "weight"].to_numpy()
    ax.hist(
        ll_m,
        bins=100,
        range=(200, 1800),
        histtype="step",
        weights=wt,
        label=f"{m} GeV"
    )
ax.set_xlabel("$m_{HH}$ [GeV]")
ax.legend()
fig.savefig("m_hh.png")
fig.savefig("m_hh.pdf")