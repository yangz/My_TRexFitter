from collections import defaultdict
import pathlib

import matplotlib.pylab as plt
import numpy as np
import ROOT
import xlwt

check_dir = pathlib.Path("stats")

# Get data
data_dict = defaultdict(lambda: defaultdict(dict))
for i, path in enumerate(
    sorted(pathlib.Path(".").rglob(f"{check_dir}/**/myLimit_BLIND_CL95.root"))
):
    # ignore non-target folders
    if not str(path).startswith(str(check_dir)):
        continue
    print("#### Checking:", path)
    mass = int(path.parts[1].split("_")[1])
    mass_inj = int(path.parts[2].split("_")[1])
    # retrive data
    root_file = ROOT.TFile.Open(path.as_posix())
    root_tree = root_file.stats
    for entry in root_tree:
        # print("+2sigma: ", entry.exp_upperlimit_plus2)
        # print("+1sigma: ", entry.exp_upperlimit_plus1)
        # print("median: ", entry.exp_upperlimit)
        # print("-1sigma: ", entry.exp_upperlimit_minus1)
        # print("-2sigma: ", entry.exp_upperlimit_minus2)
        data_dict[mass][mass_inj]["plus2"] = entry.exp_upperlimit_plus2
        data_dict[mass][mass_inj]["plus1"] = entry.exp_upperlimit_plus1
        data_dict[mass][mass_inj]["medium"] = entry.exp_upperlimit
        data_dict[mass][mass_inj]["minus1"] = entry.exp_upperlimit_minus1
        data_dict[mass][mass_inj]["minus2"] = entry.exp_upperlimit_minus2

# Debug
#for m, sub in data_dict.items():
#    print(f"#### mass={m}")
#    keys = sub.keys()
#    for k in sorted(keys):
#        value = sub[k]["medium"]
#        if m == k:
#            print(f"    > def={k}, {value}")
#        else:
#            print(f"    > inj={k}, {value}")

# Save excel
book = xlwt.Workbook(encoding="utf-8")
masses = sorted(data_dict.keys())
for m in masses:
    new_sheet = book.add_sheet(f"m_{m}")
    sub = data_dict[m]
    injects = sorted(sub.keys())
    keys = sub[injects[0]].keys()
    # write col head
    new_sheet.write(0, 0, "inject")
    for col, ky in enumerate(keys):
        new_sheet.write(0, col + 1, ky)
    # write col contents
    for row, inj in enumerate(injects):
        new_sheet.write(row + 1, 0, inj)
        for col, ky in enumerate(keys):
            new_sheet.write(row + 1, col + 1, sub[inj][ky])
book.save(check_dir / "inject_limits.xls")

# Get differences
masses = sorted(data_dict.keys())
injects = sorted(data_dict[masses[0]].keys())
n_row = len(masses)
n_col = len(injects)
diff = np.zeros((n_row, n_col))
for row, m in enumerate(masses):
    def_val = data_dict[m][m]["medium"]
    for col, inj in enumerate(injects):
        if row == col:
            diff[row][col] = np.NaN
        else:
            inj_val = data_dict[m][inj]["medium"]
            diff[row][col] = (inj_val / def_val - 1) * 100
    # print(m, diff[row])  # DEBUG
## plot heat
fig, ax = plt.subplots()
im = ax.imshow(diff)
cbar = ax.figure.colorbar(im, ax=ax)
cbar.ax.set_ylabel("diff(%)", rotation=-90, va="bottom")
for i in range(len(masses)):
    for j in range(len(injects)):
        if i == j:
            continue
        text = ax.text(j, i, f"{diff[i][j]:.2f}", ha="center", va="center", color="w")
ax.set_xticks(np.arange(len(injects)), labels=injects)
ax.set_xlabel("inject mass [GeV]")
ax.set_yticks(np.arange(len(masses)), labels=masses)
ax.set_ylabel("mass [GeV]")
ax.set_title("Impact of signal injection")
fig.tight_layout()
fig.savefig(check_dir / "inject_heat.png")
