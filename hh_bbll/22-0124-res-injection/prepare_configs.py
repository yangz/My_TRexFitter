import pathlib
from shutil import copyfile

# Constants
mass_points = [251, 260, 300, 400, 500, 600, 800, 1000]

# Create config files
with open("template.config", "r") as f:
    config_temp = f.read()
with open("template_inj.config", "r") as f:
    config_temp_inj = f.read()
for m in mass_points:
    # prepare folders
    m_dir = pathlib.Path(f"stats/m_{m:04d}")
    m_dir.mkdir(parents=True, exist_ok=True)
    # add config for each injection
    for m_inj in mass_points:
        if m_inj == m:
            content = config_temp.format(p_MASS=m)
            config_name = f"m_{m:04d}_default_{m:04d}_stats.config"
        else:
            content = config_temp_inj.format(p_MASS=m, p_MASS_inject=m_inj)
            config_name = f"m_{m:04d}_inj_{m_inj:04d}_stats.config"
        with m_dir.joinpath(config_name).open("w", encoding="utf-8") as f:
            f.write(content)
    # copy helper scripts
    copyfile(f"./fit_all.sh", m_dir.joinpath("fit_all.sh"))
