% --------------- %
% ---  JOB    --- %
% --------------- %

Job: "m_251_inj_800" 
  CmeLabel: "13 TeV"
  POI: "MuHH"
  ReadFrom: NTUP
  NtuplePaths: "/data/V09/"
  Label: "Di-Higgs"
  LumiLabel: "139 fb^{-1}"    % data17?
  MCweight: "1.0/139.0"
  Lumi: 139
  NtupleName: "Nominal"
  DebugLevel: 0
  MCstatThreshold: NONE
  % MergeUnderOverFlow: TRUE
  SystControlPlots: TRUE
  % SystErrorBars: TRUE
  HistoChecks: NOCRASH
  % StatOnly: TRUE
  SplitHistoFiles: TRUE   % set this in order to have separated .root files in Histograms/ for different regions
  ImageFormat: "png","pdf"
  DoSignalRegionsPlot: TRUE
  DoPieChartPlot: TRUE
  PlotOptions: NOXERR
  LegendNColumns: 2



% --------------- %
% ---  FIT    --- %
% --------------- %

Fit: "fit"
  FitRegion: CRSR
  FitType: SPLUSB
  POIAsimov: 1
  FitBlind: TRUE
  doLHscan: "MuHH"
  NumCPU: 15



% --------------- %
% ---  LIMIT    --- %
% --------------- %

Limit: "limit"
  LimitType: ASYMPTOTIC
  LimitBlind: TRUE


% -------------------- %
% --- Significance --- %
% -------------------- %
Significance: "significance"
  SignificanceBlind: TRUE
  POIAsimov: 1



% --------------- %
% --- REGIONS --- %
% --------------- %

Region: "ll_highm"
  Type: CONTROL
  NtuplePathSuff: "/2tag_topcr"
  Selection: "ll_m>110.0&&m_hh_truth==0"
  Variable: "ll_m",30,110,410
  VariableTitle: "m_{ll} [GeV]"
  Label: "t#bar{t} CR"
  BinWidth: 10
  LogScale: FALSE

%Region: "Region2_SR"
  %Type: SIGNAL
  %Selection: "is_sf==0&&ll_m>5"
  %Variable: "pytorch_region2_pDNN_251_classifier0",20,0,1
  %VariableTitle: "NN Score"
  %Label: "pDNN at 251 GeV, LM/DF"
  %BinWidth: 0.1
  %LogScale: FALSE  

Region: "Region3_SR"
  Type: SIGNAL
  NtuplePathSuff: "/2tag_sr1_applied"
  Selection: "is_sf==1&&ll_m<75"
  Variable: "pytorch_region3_pDNN_251_classifier0",20,0,1
  VariableTitle: "NN Score"
  Label: "pDNN at 251 GeV, LM/SF"
  BinWidth: 0.1
  LogScale: TRUE

Region: "Region4_SR"
  Type: SIGNAL
  NtuplePathSuff: "/2tag_sr1_applied"
  Selection: "is_sf==0&&ll_m<75"
  Variable: "pytorch_region4_pDNN_251_classifier0",20,0,1
  VariableTitle: "NN Score"
  Label: "pDNN at 251 GeV, LM/DF"
  BinWidth: 0.1
  LogScale: TRUE



% --------------- %
% --- SAMPLES --- %
% --------------- %

%Sample: "Data"
   %Type: DATA
   %NtupleFile: "data_all_sysweight"

% Injection
Sample: "DiHiggs_inject"
  Type: BACKGROUND
  Title: "HH_inject"
  FillColor: 3
  LineColorRGB: 0,0,0
  NtupleFile: "sig_res_sysweight"
  Selection: "m_hh_truth == 800"
  MCweight: "weight"

% Signal

Sample: "DiHiggs"
  Type: SIGNAL
  Title: "HH"
  FillColor: 3
  LineColorRGB: 0,0,0
  NtupleFile: "sig_res_sysweight"
  Selection: "m_hh_truth == 251"
  MCweight: "weight"

% Background

Sample: "ttbar"
  Type: BACKGROUND
  Title: "top"
  FillColorRGB: 7,162,171
  LineColorRGB: 0,0,0
  NtupleFiles: "bkg_ttbar_sysweight"
  %Selection: "process_id == 100"
  MCweight: "weight"

Sample: "ttV"
  Type: BACKGROUND
  Title: "top"
  FillColorRGB: 7,162,171
  LineColorRGB: 0,0,0
  NtupleFiles: "bkg_ttbarV_sysweight"
  %Selection: "process_id == 100"
  MCweight: "weight"
  
Sample: "stop"
  Type: BACKGROUND
  Title: "top"
  FillColorRGB: 7,162,171
  LineColorRGB: 0,0,0
  NtupleFiles: "bkg_stop_sysweight"
  %Selection: "process_id == 100"
  MCweight: "weight"

Sample: "DY"
  Type: BACKGROUND
  Title: "Z#\to ll"
  FillColorRGB: 244,244,111
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_Zll_sysweight"
  MCweight: "weight"

Sample: "diboson"
  Type: BACKGROUND
  Title: "diboson"
  FillColorRGB: 69,196,139
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_diboson_sysweight"
  MCweight: "weight"  % k-factor 1 or 1.7?
  
Sample: "fake"
  Type: BACKGROUND
  Title: "fake"
  FillColorRGB: 94,104,122
  LineColorRGB: 0,0,0
  NtupleFile: "bkg_fakes_sysweight"
  MCweight: "weight"  % k-factor 1 or 1.7?

Sample: "Higgs"
  Type: BACKGROUND
  Title: "Higgs"
  FillColorRGB: 244,121,66
  LineColorRGB: 244,121,66
  NtupleFile: "bkg_Higgs_sysweight"
  MCweight: "weight"  % k-factor 1 or 1.7?



% --------------- %
% - NORMFACTORS - %
% --------------- %

NormFactor: "MuHH"
   Title: "#mu (HH)"
   Nominal: 1
   Min: 0
   Max: 50
   Samples: DiHiggs
   
NormFactor: "MuDY"
   Title: "#mu (DY)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: DY
  
NormFactor: "Muttbar"
   Title: "#mu (ttbar)"
   Nominal: 1
   Min: 0
   Max: 3
   Samples: ttbar


% --------------- %
% - SYSTEMATICS - %
% --------------- %

Systematic: "LUMI"
  Title: "Luminosity"
  Type: OVERALL
  Samples: all
  OverallUp: 0.021
  OverallDown: -0.021
  Category: Lumi
  
